﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class Tablero : MonoBehaviour
{
    public Text tPanelFinJuego;
    public GameObject panelFinJuego;
    private Pieza[,] matrizPiezas = new Pieza[8, 8];
    TableroV tableroV;
    private ClickListener[,] matrizTiles = new ClickListener[8, 8];//almacena las casillas q generarán las piezas
    public GameObject casilla;
    public Text mensaje;
    public ClickListener tileInicial;
    public Camera camara;
    private bool turnoJugador;
    private float contadorTiempo;
    
    private readonly String MENSAJE_ERROR = "Jugada no válida";
    private readonly String MENSAJE_IA_DECIDIENDO = "la IA está pensando...";
    
    private bool juegoTerminado = false;
    private static int numJugadoresHumanos = 1;
    private float minDelay = 1;//numero de segundos que esperará como mínimo, la IA en responder
    
    private int numTurno;
    private bool juegoEnPausa = false;
    private enum TipoJugador { HUMANO, NUM_ESQUINAS, NUM_PIEZAS, NUM_ACCIONES_ADVERSARIO, APRENDIZAJE };
    private int numClics = 0;
    private int[] casillaInicial = new int[2];
    private Pieza piezaMovida;
    private static TipoJugador tipoJ1;
    private static TipoJugador tipoJ2;
    private static int numPartidasRestantes = 1;
    public GameObject menuCoronacion;
    private static Tablero tablero;
    private bool jaqueDeclarado;
    private int[] coordenadasPromocion = new int[2];

    
   
    public bool IAEnPausa;
    private bool desactivarCapturaAlPaso;
    
    private Queue<PiezaVirtual> colaPiezasConMovEspReciente;
    private Queue<bool> colaTurnosMovEspActivado;
    
    
    public void promocion(String nombrePieza)
    {
        ClickListener tile = matrizTiles[coordenadasPromocion[0], coordenadasPromocion[1]];
        Pieza piezaOriginal = tile.getPiezaColocada();
        Pieza piezaNueva;
        tile.retirarPiezaDeCasilla();
        switch (nombrePieza)
        {
            case "DAMA":
                piezaNueva =tile.colocarPieza(!turnoJugador, TipoPieza.DAMA);
                tableroV.promocionarPiezaVirtual(piezaOriginal.getPiezaVirtual(), piezaNueva.getPiezaVirtual());
                break;
            case "TORRE":
                piezaNueva = tile.colocarPieza(!turnoJugador, TipoPieza.TORRE);
                tableroV.promocionarPiezaVirtual(piezaOriginal.getPiezaVirtual(), piezaNueva.getPiezaVirtual());
                break;
            case "ALFIL":
                piezaNueva = tile.colocarPieza(!turnoJugador, TipoPieza.ALFIL);
                tableroV.promocionarPiezaVirtual(piezaOriginal.getPiezaVirtual(), piezaNueva.getPiezaVirtual());
                break;
            case "CABALLO":
                piezaNueva = tile.colocarPieza(!turnoJugador, TipoPieza.CABALLO);
                tableroV.promocionarPiezaVirtual(piezaOriginal.getPiezaVirtual(), piezaNueva.getPiezaVirtual());
                break;
        }
        
    }
    private void mostrarMenuCoronacion()
    {
        menuCoronacion.SetActive(true);
    }
    public static Tablero getTablero()
    {
        if(tablero == null)
        {
            tablero = new Tablero();
        }
        return tablero;
    }
    private void limpiarTablero() {
        foreach(Pieza p in matrizPiezas){
            if(p!= null)
            {
                Destroy(p.gameObject);
            }
           
        }
        matrizPiezas = new Pieza[8,8];
      //  matrizHuecosVecinos = new ArraySet<int>();
    }
    public void comunicarMovEspecialReciente(PiezaVirtual p)
    {
        colaPiezasConMovEspReciente.Enqueue(p);//necesito una cola
        colaTurnosMovEspActivado.Enqueue(turnoJugador);//necesito una cola
        
    }
    public void nuevoJuego()
    {
        limpiarTablero();
        generarTablero();
        juegoTerminado = false;
        turnoJugador = true;//TRUE = Jugador 1
        ocultarMensaje();
        numTurno = 0;
        IAEnPausa = false;
        colaPiezasConMovEspReciente = new Queue<PiezaVirtual>();//necesito una cola
        colaTurnosMovEspActivado = new Queue<bool>();
        comenzarTurno();

    }
    public void pausarReanudarJuego()
    {
        juegoEnPausa = !juegoEnPausa;
    }
   
    private void generarCasillas()
    {

        Vector3 posicionInicial = casilla.transform.position;
        int fila, columna;
        tileInicial.SetFilaYColumna(0, 0);
        tileInicial.setTablero(this);
        matrizTiles[0, 0] = tileInicial;
        for (int i = 1; i < 64; i++)
        {
            fila = (int)Mathf.Floor(i / 8);
            columna = i % 8;            
            casilla = Instantiate(casilla);
            casilla.transform.position = posicionInicial + new Vector3(1,0,0) *1.33f *(columna) + new Vector3(0, 0, 1)  *-1.33f* ( fila);
            ClickListener tile = casilla.GetComponent<ClickListener>();
            tile.SetFilaYColumna(fila, columna);
            tile.setTablero(this);
            matrizTiles[fila, columna] = tile;
        }
    }

    public void setPiezaCapturadaAlPaso(int fila, int columna)
    {
        matrizTiles[fila, columna].retirarPiezaDeCasilla();
    }
    private void generarPeonesIniciales()
    {
        Pieza p;
        for (int i = 0; i < 8; i++)
        {
            //peon blanco
            p = matrizTiles[6, i].colocarPieza(true, TipoPieza.PEON);
            p.setTablero(this);//cuando se hayan implementado los estados, hacer q dependa de la clase Estado, en lugar de tablero
            tableroV.registrarPiezaVirtualBlanca(p.getPiezaVirtual(), new TuplaCoordenadas(6,i));

            //peon negro
            p = matrizTiles[1, i].colocarPieza(false, TipoPieza.PEON);
            p.setTablero(this);
            tableroV.registrarPiezaVirtualNegra(p.getPiezaVirtual(), new TuplaCoordenadas(1,i));

        }
    }
   
    private void generarTorresCaballosYAlfilesIniciales()
    {
        Pieza p;
        TipoPieza[] piezasM = { TipoPieza.TORRE, TipoPieza.CABALLO, TipoPieza.ALFIL };
        //torres, caballos y alfiles
        for (int i = 0; i < 3; i++)
        {
            //lado izdo
            p = matrizTiles[7, i].colocarPieza(true, piezasM[i]);
            tableroV.registrarPiezaVirtualBlanca(p.getPiezaVirtual(), new TuplaCoordenadas(7,i));
            p = matrizTiles[0, i].colocarPieza(false, piezasM[i]);
            tableroV.registrarPiezaVirtualNegra(p.getPiezaVirtual(), new TuplaCoordenadas(0, i));
            //lado dcho
            p = matrizTiles[7, 7 - i].colocarPieza(true, piezasM[i]);
            tableroV.registrarPiezaVirtualBlanca(p.getPiezaVirtual(), new TuplaCoordenadas(7, 7- i));
            p = matrizTiles[0, 7 - i].colocarPieza(false, piezasM[i]);
            tableroV.registrarPiezaVirtualNegra(p.getPiezaVirtual(), new TuplaCoordenadas(0, 7- i));
        }
    }
    private void generarReyes()
    {
        Pieza p;
        p = matrizTiles[7, 4].colocarPieza(true, TipoPieza.REY);
        tableroV.setReyBlanco(p.getPiezaVirtual());
        p.setTablero(this);
        tableroV.registrarPiezaVirtualBlanca(p.getPiezaVirtual(), new TuplaCoordenadas(7, 3));
        p = matrizTiles[0, 4].colocarPieza(false, TipoPieza.REY);
        tableroV.setReyNegro(p.getPiezaVirtual());
        p.setTablero(this);
        tableroV.registrarPiezaVirtualNegra(p.getPiezaVirtual(), new TuplaCoordenadas(0, 3));
    }
    private void generarReinasIniciales()
    {
        Pieza p;
        p = matrizTiles[7, 3].colocarPieza(true, TipoPieza.DAMA);
        tableroV.registrarPiezaVirtualBlanca(p.getPiezaVirtual(), new TuplaCoordenadas(7, 4));        
        p = matrizTiles[0, 3].colocarPieza(false, TipoPieza.DAMA);        
        tableroV.registrarPiezaVirtualNegra(p.getPiezaVirtual(), new TuplaCoordenadas(0, 4));
    }
    private void generarTablero()
    {
        juegoTerminado = true;
        generarPeonesIniciales();
        generarTorresCaballosYAlfilesIniciales();
        generarReyes();
        generarReinasIniciales();
        juegoTerminado = false;
    }
  
    private void mostrarMensaje(String textoMensaje)
    {
        mensaje.gameObject.SetActive(true);
        mensaje.text = textoMensaje;

    }
    private void ocultarMensaje()
    {
        mensaje.gameObject.SetActive(false);
        contadorTiempo = 0;
    }
    private void Start()
    {
        tableroV = new TableroV();
        //matrizBots = new IABot[2];
        tablero = this;
        generarCasillas();
        generarTablero();
        turnoJugador = true;//TRUE = Jugador 1
        ocultarMensaje();
        colaPiezasConMovEspReciente = new Queue<PiezaVirtual>();
        colaTurnosMovEspActivado = new Queue<bool>();
        numTurno = 0;
        //IAEnPausa = false;
        comenzarTurno();
    }
    void Update()
    {
        contadorTiempo+= Time.deltaTime;//de esta forma se ajusta mejor a las distintas velocidades de los equipos en los que se ejecute el juego
        if (!juegoTerminado && contadorTiempo > 2 )
        {
            ocultarMensaje();
        }
        if (Input.GetKeyUp(KeyCode.Escape))//cierra el juego
        {
            Application.Quit();
        }

    }


    public bool IsTurnoJugador()
    {
        return turnoJugador;
    }

    private void comenzarTurno()
    {
        if (comprobarFinPartida() == false && tableroV.HayJaque(tableroV.obtenerReyDeJugador(turnoJugador)))
        {
            Debug.Log("jaque al rey");
            mostrarMensaje("jaque al rey");
            jaqueDeclarado = true;
            //esJaqueMate();
        }
        else
        {
            jaqueDeclarado = false;
        }
    }
    private void cambiarTurno()
    {
      
        numTurno++;
        turnoJugador = !turnoJugador;//se pasa al siguiente turno
        
        comenzarTurno();
    }
 
    /// <summary>
    /// Comprueba que es legal colocar una pieza sobre la casilla sobre la que se ha hecho click
    /// </summary>
    /// <param name="tile">Listener exclusivo para cada casilla</param>
    /// <param name="fila">número de fila en la que está la casilla</param>
    /// <param name="columna">número de columna sobre la que está la casilla</param>
    public void comprobarValidezClickJugada(ClickListener tile,int fila, int columna)//actualizar esquema
    {
        //Debug.Log("juego en pausa:" + juegoEnPausa);
        if (!juegoEnPausa) {       
            if(numClics < 1)
            {
                recogerClickInicial(tile);
            }
            else
            {
                int[] casillaDestino = { fila,columna};
                //if jugada válida
                if((casillaInicial[0]!= fila || casillaInicial[1]!= columna) && piezaMovida != null && piezaMovida.getPiezaVirtual().comprobarValidezJugada(casillaInicial, casillaDestino,tableroV))
                {
                    if (tableroV.esEnroque(piezaMovida.getPiezaVirtual(), casillaInicial[1], casillaDestino[1])){
                        moverPieza(tile, new TuplaCoordenadas(casillaInicial[0], casillaInicial[1]), new TuplaCoordenadas(fila, columna));
                        if (tableroV.esEnroqueCorto(piezaMovida.getPiezaVirtual(), casillaInicial[1], casillaDestino[1]))
                        {
                            matrizTiles[casillaInicial[0], 7].retirarPiezaDeCasilla();
                           Pieza torreMovida = matrizTiles[casillaDestino[0], casillaDestino[1] -1].colocarPieza(turnoJugador, TipoPieza.TORRE, true);
                            torreMovida.setMatrixIndexes(casillaDestino[0], casillaDestino[1] - 1);
                            //moverPieza(matrizTiles[casillaInicial[0],7], new TuplaCoordenadas(casillaInicial[0], 7), new TuplaCoordenadas(fila, columna - 1));
                        }
                        else if (tableroV.esEnroqueLargo(piezaMovida.getPiezaVirtual(), casillaInicial[1], casillaDestino[1]))
                        {
                            matrizTiles[casillaInicial[0], 0].retirarPiezaDeCasilla();
                            Pieza torreMovida = matrizTiles[casillaDestino[0], casillaDestino[1] + 1].colocarPieza(turnoJugador, TipoPieza.TORRE, true);
                            torreMovida.setMatrixIndexes(casillaDestino[0], casillaDestino[1] + 1);
                        }
                    }
                    else
                    {
                        if (EstaOcupada(tile))
                        {
                            comerPiezaDeCasilla(tile);
                        }
                        moverPieza(tile, new TuplaCoordenadas(casillaInicial[0], casillaInicial[1]), new TuplaCoordenadas(fila, columna));
                    }

                    actualizaColaPiezasConMovimientoEspecialReciente();
                    TipoPieza tipoPiezaMovida = this.piezaMovida.getTipo();
                    if (tipoPiezaMovida.ToString().Equals("PEON") && (fila == 0 || fila == 7))
                    {
                        mostrarMenuCoronacion();
                        coordenadasPromocion[0] = fila;
                        coordenadasPromocion[1] = columna;                        
                    }
                    cambiarTurno();
                }
                else
                {
                    mostrarMensaje(MENSAJE_ERROR);
                }
                numClics = 0;
            }        
        }
    }
    private void actualizaColaPiezasConMovimientoEspecialReciente()
    {
        if (colaPiezasConMovEspReciente.Count > 0 && colaTurnosMovEspActivado.Peek() != turnoJugador)
        {
            colaPiezasConMovEspReciente.Dequeue().actualizarMovEspRec();
            colaTurnosMovEspActivado.Dequeue();
        }
    }
    private void moverPieza(ClickListener tile,TuplaCoordenadas posicionInicial, TuplaCoordenadas posicionFinal)
    {
        matrizTiles[posicionInicial.getX(), posicionInicial.getY()].retirarPiezaDeCasilla();
        tile.colocarPieza(turnoJugador, this.piezaMovida.getTipo(), true);
        Debug.Log("pieza movida: " + this.piezaMovida.getTipo());
        tableroV.moverPiezaVirtual(posicionInicial, posicionFinal);
        this.piezaMovida.setMatrixIndexes(posicionFinal.getX(), posicionFinal.getY());
    }
    

    private void recogerClickInicial(ClickListener tile)
    {
        piezaMovida = tile.getPiezaColocada();
        if (piezaMovida != null && turnoJugador == piezaMovida.getOwner())
        {
            casillaInicial[0] = tile.getFila();
            casillaInicial[1] = tile.getColumna();
            TipoPieza texto = piezaMovida.getTipo();
            Debug.Log("pieza movida: " + texto);
            Debug.Log("dueño: " + piezaMovida.getOwner());
            Debug.Log("clic inicial en fila " + tile.getFila() + ", columna " + tile.getColumna());
            numClics++;
        }
        else
        {
            mostrarMensaje(MENSAJE_ERROR);
        }
    }

    
    private void comerPiezaDeCasilla(ClickListener tile)
    {
        Pieza piezaComida = tile.getPiezaColocada();
        PiezaVirtual piezaVirtualComida = piezaComida.getPiezaVirtual();
        tableroV.eliminarPiezaVirtualComida(piezaVirtualComida);
        tile.retirarPiezaDeCasilla();
    }

   
    private bool EstaOcupada(ClickListener tile)
    {
        return tile.getPiezaColocada() != null;
    }
    public bool comprobarFinPartida()// para jugador vs IA, jugador vs jugador
    {
        if (!tableroV.hayJugadas(turnoJugador))
        {
            bool turnoJugador2 = !turnoJugador;
                if (tableroV.HayJaque(!turnoJugador))//si en el turno anterior jugó el jugador 1
                {
                    mostrarMensaje("ha ganado el jugador 2");
                }
                else if(tableroV.HayJaque(turnoJugador))
                {
                    mostrarMensaje("ha ganado el jugador 1");
                }
            
            else //si el jugador actual se ha quedado sin jugadas, sin que le hayan dado jaque
            {
                mostrarMensaje("tablas por rey ahogado");
            }
            juegoTerminado = true;
            return true;
        }
        return false;
    }
}
