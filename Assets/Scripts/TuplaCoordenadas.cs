﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuplaCoordenadas {

    private int x;
    private int y;

    public TuplaCoordenadas()
    {

    }
    public TuplaCoordenadas(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public void setX(int x)
    {
        this.x = x;
    }
    public int getX()
    {
        return x;
    }
    public void setY(int y)
    {
        this.y = y;
    }
    public int getY()
    {
        return y;
    }


}
