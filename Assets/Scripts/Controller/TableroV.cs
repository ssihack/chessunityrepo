﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableroV {
    protected PiezaVirtual[,] matrizPiezasV = new PiezaVirtual[8, 8];
    private List<PiezaVirtual> listaPiezasJBlanco;
    private List<PiezaVirtual> listaPiezasJNegro;
    private ReyV reyBlanco;
    private ReyV reyNegro;
    public TableroV()
    {
        matrizPiezasV = new PiezaVirtual[8, 8];
        listaPiezasJBlanco = new List<PiezaVirtual>();
        listaPiezasJNegro = new List<PiezaVirtual>();
    }
    public List<PiezaVirtual> getListaPiezasJBlanco()
    {
        return listaPiezasJBlanco;
    }
    public List<PiezaVirtual> getListaPiezasJNegro()
    {
        return listaPiezasJNegro;
    }
    public PiezaVirtual[,] getMatrizPiezasV()
    {
        return matrizPiezasV;
    }
    public bool esEnroque(PiezaVirtual pieza, int columnaOrigen, int columnaDestino)
    {
        return pieza is ReyV && isDestinoADosCasillasDeDistancia(columnaOrigen, columnaDestino);
    }
    public bool esEnroqueCorto(PiezaVirtual pieza, int columnaOrigen, int columnaDestino)
    {
        return esEnroque(pieza,columnaOrigen, columnaDestino) && columnaDestino > columnaOrigen;
    }
    public bool esEnroqueLargo(PiezaVirtual pieza, int columnaOrigen, int columnaDestino)
    {
        return esEnroque(pieza, columnaOrigen, columnaDestino) && columnaDestino < columnaOrigen;
    }
    public bool isReachableByEnemyKnight(bool turnoJugador, int numFila, int numColumna)
    {
        
        PiezaVirtual pV = null;
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.Add(new TuplaCoordenadas(numFila + 2, numColumna + 1));
        coordenadas.Add(new TuplaCoordenadas(numFila + 2, numColumna - 1));
        coordenadas.Add(new TuplaCoordenadas(numFila - 2, numColumna + 1));
        coordenadas.Add(new TuplaCoordenadas(numFila - 2, numColumna - 1));
        coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna + 2));
        coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna - 2));
        coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna + 2));
        coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna - 2));
        coordenadas = descartarCoordenadasFueraDelTablero(coordenadas);
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.CABALLO))
            {
                return true;
            }
        }
        return false;
    }
    public List<TuplaCoordenadas> descartarCoordenadasFueraDelTablero(List<TuplaCoordenadas> lista)
    {
        List<TuplaCoordenadas> listaNueva = new List<TuplaCoordenadas>();
        foreach (TuplaCoordenadas t in lista)
        {
            if (t.getX() <= 7 && t.getX() >= 0 && t.getY() <= 7 && t.getY() >= 0)
            {
                listaNueva.Add(t);
            }
        }
        return listaNueva;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueDiagonalesPrincipales(TuplaCoordenadas coordenadasCasillaAmenazada, bool turnoJugador)
    {

        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 1; i < 8; i++)
        {

            if (coordenadasCasillaAmenazada.getX() + i > 7 || coordenadasCasillaAmenazada.getY() + i > 7 ||
                matrizPiezasV[coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() + i] != null &&
                matrizPiezasV[coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() + i].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() + i);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {
            if (coordenadasCasillaAmenazada.getX() - i < 0 || coordenadasCasillaAmenazada.getY() - i < 0 ||
                matrizPiezasV[coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() - i] != null &&
                matrizPiezasV[coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() - i].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() - i);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueDiagonalesPrincipales(TuplaCoordenadas coordenadasCasillaAmenazada, PiezaVirtual piezaQuePregunta,bool turnoJugador)
    {
         int filaCasillaAmenazada = coordenadasCasillaAmenazada.getX();
         int columnaCasillaAmenazada = coordenadasCasillaAmenazada.getY();

        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 1; i < 8; i++)
        {

            if (coordenadasCasillaAmenazada.getX() + i > 7 || coordenadasCasillaAmenazada.getY() + i > 7 ||
                matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada + i] != null &&
                (matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada + i].getOwner() == turnoJugador) 
                && !matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada + i].Equals(piezaQuePregunta))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() + i);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {
            if (coordenadasCasillaAmenazada.getX() - i < 0 || coordenadasCasillaAmenazada.getY() - i < 0 ||
                matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada - i] != null &&
                (matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada - i].getOwner() == turnoJugador 
                && !matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada - i].Equals(piezaQuePregunta)))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() - i);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    bool IsTileOutsideOfBoardGame(TuplaCoordenadas coordenadas)
    {
        if (coordenadas.getX() < 0 || coordenadas.getY() < 0 || coordenadas.getX() > 7 || coordenadas.getY() > 7)
        {
            return true;
        }return false;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueDiagonalesInversas(TuplaCoordenadas coordenadasCasillaAmenazada, bool turnoJugador)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 0; i < 8; i++)
        {
            if (coordenadasCasillaAmenazada.getX() + i > 7 || coordenadasCasillaAmenazada.getY() - i < 0 || matrizPiezasV[coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() - i] != null && matrizPiezasV[coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() - i].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() - i);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {

            if (coordenadasCasillaAmenazada.getX() - i < 0 || coordenadasCasillaAmenazada.getY() + i > 7 || matrizPiezasV[coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() + i] != null && matrizPiezasV[coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() + i].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() + i);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueDiagonalesInversas(TuplaCoordenadas coordenadasCasillaAmenazada, PiezaVirtual piezaQuePregunta,bool turnoJugador)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        int filaCasillaAmenazada = coordenadasCasillaAmenazada.getX();
        int columnaCasillaAmenazada = coordenadasCasillaAmenazada.getY();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 0; i < 8; i++)
        {
            if (filaCasillaAmenazada + i > 7 || columnaCasillaAmenazada - i < 0 || matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada - i] != null 
               && matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada - i].getOwner() == turnoJugador
               && !matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada - i].Equals(piezaQuePregunta))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY() - i);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {

            if (filaCasillaAmenazada - i < 0 || columnaCasillaAmenazada + i > 7 || 
                matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada + i] != null 
                && matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada + i].getOwner() == turnoJugador
                && !matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada + i].Equals(piezaQuePregunta))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY() + i);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public bool isReachableByEnemyBishop(bool turnoJugador, int numFila, int numColumna)
    {
        PiezaVirtual pV = null;
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesPrincipales(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesInversas(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.ALFIL))
            {
                return true;
            }
        }
        return false;
    }

    public bool isReachableByEnemyBishop(bool turnoJugador, PiezaVirtual piezaQuePregunta,int numFila, int numColumna)
    {
        PiezaVirtual pV = null;
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesPrincipales(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta, turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesInversas(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta, turnoJugador));
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.ALFIL))
            {
                return true;
            }
        }
        return false;
    }

    private bool coordinatessBelongToTheBoard(TuplaCoordenadas coordenadas)
    {
        return !(coordenadas.getY() < 0 || coordenadas.getY() > 7 || coordenadas.getX() < 0 || coordenadas.getX() > 7);
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueHorizontales(TuplaCoordenadas coordenadasCasillaAmenazada, bool turnoJugador)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 1; i < 8; i++)
        {

            if (coordenadasCasillaAmenazada.getY() - i < 0 || matrizPiezasV[coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() - i] != null && matrizPiezasV[coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() - i].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() - i);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {

            if (coordenadasCasillaAmenazada.getY() + i > 7 || (matrizPiezasV[coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() + i] != null && matrizPiezasV[coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() + i].getOwner() == turnoJugador))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() + i);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueHorizontales(TuplaCoordenadas coordenadasCasillaAmenazada, PiezaVirtual piezaQuePregunta,bool turnoJugador)
    {
        int filaCasillaAmenazada = coordenadasCasillaAmenazada.getX();
        int columnaCasillaAmenazada = coordenadasCasillaAmenazada.getY();
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 1; i < 8; i++)
        {

            if (columnaCasillaAmenazada - i < 0 || matrizPiezasV[filaCasillaAmenazada, columnaCasillaAmenazada - i] != null 
                && matrizPiezasV[filaCasillaAmenazada, columnaCasillaAmenazada - i].getOwner() == turnoJugador
                && !matrizPiezasV[filaCasillaAmenazada, columnaCasillaAmenazada - i].Equals(piezaQuePregunta))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX(), coordenadasCasillaAmenazada.getY() - i);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {

            if (columnaCasillaAmenazada + i > 7 || (matrizPiezasV[filaCasillaAmenazada, columnaCasillaAmenazada + i] != null 
                && matrizPiezasV[filaCasillaAmenazada, columnaCasillaAmenazada + i].getOwner() == turnoJugador
                && !matrizPiezasV[filaCasillaAmenazada, columnaCasillaAmenazada + i].Equals(piezaQuePregunta)))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX(), columnaCasillaAmenazada + i);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueVerticales(TuplaCoordenadas coordenadasCasillaAmenazada, bool turnoJugador)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 1; i < 8; i++)
        {

            if (coordenadasCasillaAmenazada.getX() + i > 7 || matrizPiezasV[coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY()] != null && matrizPiezasV[coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY()].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() + i, coordenadasCasillaAmenazada.getY());
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {
            if (coordenadasCasillaAmenazada.getX() - i < 0 || matrizPiezasV[coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY()] != null && matrizPiezasV[coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY()].getOwner() == turnoJugador)
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(coordenadasCasillaAmenazada.getX() - i, coordenadasCasillaAmenazada.getY());
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public List<TuplaCoordenadas> obtenerCoordenadasDeAtaqueVerticales(TuplaCoordenadas coordenadasCasillaAmenazada, PiezaVirtual piezaQuePregunta, bool turnoJugador)
    {
        int filaCasillaAmenazada = coordenadasCasillaAmenazada.getX();
        int columnaCasillaAmenazada = coordenadasCasillaAmenazada.getY();
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!coordinatessBelongToTheBoard(coordenadasCasillaAmenazada)) return coordenadas;
        for (int i = 1; i < 8; i++)
        {

            if (filaCasillaAmenazada + i > 7 || matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada] != null
                && matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada].getOwner() == turnoJugador
                && !matrizPiezasV[filaCasillaAmenazada + i, columnaCasillaAmenazada].Equals(piezaQuePregunta))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(filaCasillaAmenazada + i, columnaCasillaAmenazada);
            coordenadas.Add(tuplaCoordenadas);
        }
        for (int i = 1; i < 8; i++)
        {
            if (filaCasillaAmenazada - i < 0 || matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada] != null 
                && matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada].getOwner() == turnoJugador
                && !matrizPiezasV[filaCasillaAmenazada - i, columnaCasillaAmenazada].Equals(piezaQuePregunta))
            {
                break;
            }
            TuplaCoordenadas tuplaCoordenadas = new TuplaCoordenadas(filaCasillaAmenazada - i, columnaCasillaAmenazada);
            coordenadas.Add(tuplaCoordenadas);
        }
        return coordenadas;
    }

    public bool isReachableByEnemyTower(bool turnoJugador, int numFila, int numColumna)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueVerticales(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueHorizontales(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        PiezaVirtual pV = null;
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.TORRE))
            {
                return true;
            }
        }
        return false;
    }

    public bool isReachableByEnemyTower(bool turnoJugador, PiezaVirtual piezaQuePregunta, int numFila, int numColumna)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueVerticales(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta,turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueHorizontales(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta,turnoJugador));
        PiezaVirtual pV = null;
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.TORRE))
            {
                return true;
            }
        }
        return false;
    }

    public bool isReachableByEnemyQueen(bool turnoJugador, int numFila, int numColumna)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueVerticales(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueHorizontales(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesPrincipales(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesInversas(new TuplaCoordenadas(numFila, numColumna), turnoJugador));
        PiezaVirtual pV = null;
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.DAMA))
            {
                return true;
            }
        }
        return false;
    }

    public bool isReachableByEnemyQueen(bool turnoJugador, PiezaVirtual piezaQuePregunta,int numFila, int numColumna)
    {
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueVerticales(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta,turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueHorizontales(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta,turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesPrincipales(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta,turnoJugador));
        coordenadas.AddRange(obtenerCoordenadasDeAtaqueDiagonalesInversas(new TuplaCoordenadas(numFila, numColumna), piezaQuePregunta,turnoJugador));
        PiezaVirtual pV = null;
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.DAMA))
            {
                return true;
            }
        }
        return false;
    }

    public bool isReachableByEnemyKing(bool turnoJugador, int numFila, int numColumna)
    {
        Debug.Log("alcanzable por rey enemigo: ");
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna));
        coordenadas.Add(new TuplaCoordenadas(numFila, numColumna));
        coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna));
        coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna - 1));
        coordenadas.Add(new TuplaCoordenadas(numFila, numColumna - 1));
        coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna - 1));
        coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna + 1));
        coordenadas.Add(new TuplaCoordenadas(numFila, numColumna + 1));
        coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna + 1));
        PiezaVirtual pV;
        foreach (TuplaCoordenadas c in coordenadas)
        {
            if (!(c.getX() < 0 || c.getX() > 7 || c.getY() < 0 || c.getY() > 7))
            {
                pV = matrizPiezasV[c.getX(), c.getY()];
                if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.REY))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool isReachableByEnemyPawn(bool turnoJugador, int numFila, int numColumna)
    {
        PiezaVirtual pV = null;
        List<TuplaCoordenadas> coordenadas = new List<TuplaCoordenadas>();
        if (!turnoJugador)
        {
            coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna + 1));
            coordenadas.Add(new TuplaCoordenadas(numFila - 1, numColumna - 1));
        }
        else
        {
            coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna + 1));
            coordenadas.Add(new TuplaCoordenadas(numFila + 1, numColumna - 1));
        }
        coordenadas = descartarCoordenadasFueraDelTablero(coordenadas);
        foreach (TuplaCoordenadas t in coordenadas)
        {
            pV = matrizPiezasV[t.getX(), t.getY()];
            if (pV != null && pV.getOwner() != turnoJugador && pV.getTipo().Equals(TipoPieza.PEON))
            {
                return true;
            }
        }
        return false;
    }
    public void eliminarPiezaVirtualComida(PiezaVirtual piezaVirtualComida)
    {
        if (piezaVirtualComida.getOwner())
        {
            listaPiezasJBlanco.Remove(piezaVirtualComida);
            Debug.Log("al blanco le quedan " + listaPiezasJBlanco.Count + " piezas");
        }
        else
        {
            listaPiezasJNegro.Remove(piezaVirtualComida);
            Debug.Log("al negro le quedan " + listaPiezasJNegro.Count + " piezas");
        }
    }
    public void promocionarPiezaVirtual(PiezaVirtual piezaMovida, PiezaVirtual piezaNueva)
    {
        matrizPiezasV[piezaNueva.getPosX(), piezaNueva.getPosY()] = piezaNueva;
        if (piezaMovida.getOwner())
        {
            listaPiezasJBlanco.Remove(piezaMovida);
            listaPiezasJBlanco.Add(piezaNueva);
        }
        else
        {
            listaPiezasJNegro.Remove(piezaMovida);
            listaPiezasJNegro.Add(piezaNueva);
        }
    }
    public bool hayJugadas(bool jugador)
    {
        
        {
            if (jugador)
            {
                foreach (PiezaVirtual p in listaPiezasJBlanco)
                {
                    if (p != null)
                    {
                        if (p.tieneJugadas(this))
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                foreach (PiezaVirtual p in listaPiezasJNegro)
                {
                    if (p.tieneJugadas(this))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public void registrarPiezaVirtualBlanca(PiezaVirtual piezaVirtual, TuplaCoordenadas coordenadasIniciales)
    {
        listaPiezasJBlanco.Add(piezaVirtual);
        matrizPiezasV[coordenadasIniciales.getX(), coordenadasIniciales.getY()] = piezaVirtual;
    }
    public void registrarPiezaVirtualNegra(PiezaVirtual piezaVirtual, TuplaCoordenadas coordenadasIniciales)
    {
        listaPiezasJNegro.Add(piezaVirtual);
        matrizPiezasV[coordenadasIniciales.getX(), coordenadasIniciales.getY()] = piezaVirtual;
    }
    public void moverPiezaVirtual(TuplaCoordenadas posicionInicial, TuplaCoordenadas posicionFinal)
    {
        PiezaVirtual piezaV = matrizPiezasV[posicionInicial.getX(), posicionInicial.getY()];
        Debug.Log("La pieza está en la fila: " + posicionInicial.getX() + ", y la columna: " + posicionInicial.getY());
        Debug.Log("Mueve a la fila: " + posicionFinal.getX() + ", y la columna: " + posicionFinal.getY());
        matrizPiezasV[posicionInicial.getX(), posicionInicial.getY()] = null;
        matrizPiezasV[posicionFinal.getX(), posicionFinal.getY()] = piezaV;
        piezaV.setMatrixIndexes(posicionFinal.getX(), posicionFinal.getY());
    }

    /// <summary>
    /// detecta si la casilla está amenazada por cualquiera de las piezas del contrario
    /// </summary>
    /// <param name="turnoJugador"> el turno del jugador activo en el momento de la comprobación</param>
    /// <param name="numFila"></param>
    /// <param name="numColumna"></param>
    /// <returns></returns>
    public bool IsCasillaAmenazada(bool turnoJugador, int numFila, int numColumna)
    {
        //comprobación de horizontales
        //comprobación de verticales
        //comprobación de diagonales
        //comprobación de movimientos del caballo
        if (isReachableByEnemyKnight(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyPawn(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyBishop(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyTower(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyQueen(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyKing(turnoJugador, numFila, numColumna))
        {
            return true;
        }
        return false;
    }
    public bool isDestinoADosCasillasDeDistancia(int columnaOrigen, int columnaDestino)
    {
        return Mathf.Abs(columnaOrigen - columnaDestino) == 2;
    }
    public bool IsCasillaAmenazada(bool turnoJugador, PiezaVirtual piezaQuePregunta, int numFila, int numColumna)
    {
        
        //comprobación de horizontales
        //comprobación de verticales
        //comprobación de diagonales
        //comprobación de movimientos del caballo
        if (isReachableByEnemyKnight(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyPawn(turnoJugador, numFila, numColumna) ||
            isReachableByEnemyBishop(turnoJugador, piezaQuePregunta,numFila, numColumna) ||
            isReachableByEnemyTower(turnoJugador, piezaQuePregunta, numFila, numColumna) ||
            isReachableByEnemyQueen(turnoJugador, piezaQuePregunta,numFila, numColumna) ||
            isReachableByEnemyKing(turnoJugador, numFila, numColumna))
        {
            return true;
        }
        return false;
    }
    public bool HayJaque(ReyV piezaRey)
    {
        bool esJaque = IsCasillaAmenazada(piezaRey.getOwner(), piezaRey,piezaRey.getMatrixIndexes()[0], piezaRey.getMatrixIndexes()[1]);
        return esJaque;
    }
    public bool HayJaque(bool turnoJugador)
    {
       ReyV reyV = obtenerReyDeJugador(turnoJugador);
        return HayJaque(reyV);
    }
    public ReyV obtenerReyDeJugador(bool turnoJugador)
    {
        if (turnoJugador)
        {
            return reyBlanco;
        }
        return reyNegro;
    }
    public TableroV Clone()
    {
        TableroV tableroV = new TableroV();
        PiezaVirtual piezaClonada;
        foreach(PiezaVirtual p in this.listaPiezasJBlanco)
        {
            piezaClonada = p.Clone();
            tableroV.listaPiezasJBlanco.Add(piezaClonada);
            tableroV.matrizPiezasV[piezaClonada.getPosX(), piezaClonada.getPosY()] = piezaClonada;
        }
        foreach (PiezaVirtual p in this.listaPiezasJNegro)
        {
            piezaClonada = p.Clone();
            tableroV.listaPiezasJNegro.Add(piezaClonada);
            tableroV.matrizPiezasV[piezaClonada.getPosX(), piezaClonada.getPosY()] = piezaClonada;
        }
        tableroV.reyNegro = this.reyNegro;
        tableroV.reyBlanco = this.reyBlanco;
        return tableroV;
        
    }
    public void setReyBlanco(PiezaVirtual reyBlanco)
    {
        this.reyBlanco = (ReyV)reyBlanco;
    }
    public void setReyNegro(PiezaVirtual reyNegro)
    {
        this.reyNegro = (ReyV)reyNegro;
    }
    public TableroV generarSiguienteEstado(int[] origen, int[] destino, PiezaVirtual piezaVirtual)
    {
        TableroV siguienteEstado = this.Clone();
        PiezaVirtual[,] nuevoTablero = siguienteEstado.getMatrizPiezasV();
        nuevoTablero[origen[0], origen[1]] = null;
        PiezaVirtual nuevoPeon = piezaVirtual.Clone();
        nuevoPeon.setMatrixIndexes(destino[0], destino[1]);
        nuevoTablero[destino[0], destino[1]] = nuevoPeon;
        return siguienteEstado;
    }
    public bool HayJaqueEnSiguienteEstado(int[] origen, int[] destino, PiezaVirtual piezaVirtual)
    {
        if(IsTileOutsideOfBoardGame(new TuplaCoordenadas(destino[0], destino[1])))
        {
            return false;
        }
        TableroV siguienteEstado =generarSiguienteEstado(origen, destino, piezaVirtual);
        return siguienteEstado.HayJaque(piezaVirtual.getOwner());
    }
}
