﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasillaTablero  {

    private PiezaVirtual pieza;//pieza colocada sobre la casilla. No cuenta como pieza controlando la casilla
    private List<PiezaVirtual> listaBlancasControlando;
    private List<PiezaVirtual> listaNegrasControlando;
    public bool isEmpty()
    {
        return (pieza != null)? true : false;
    }
    public PiezaVirtual getPieza()
    {
        return pieza;
    }
    public void setPieza(PiezaVirtual p)
    {
        this.pieza = p;
    }
    public bool isControladaPorBlancas()
    {
        return (listaBlancasControlando.Count > 0) ? true : false;
    }
    public bool isControladaPorNegras()
    {
        return (listaBlancasControlando.Count > 0) ? true : false;
    }
    public void retirarControlDePieza(PiezaVirtual p, bool owner)
    {
        if (owner)//si jugador 1
        {
            listaBlancasControlando.Remove(p);
        }
        else
        {
            listaNegrasControlando.Remove(p);
        }
    }
    public void añadirControlDePieza(PiezaVirtual p, bool owner)
    {
        if (owner)//si jugador 1
        {
            listaBlancasControlando.Remove(p);
        }
        else
        {
            listaNegrasControlando.Remove(p);
        }
    }
    
}
