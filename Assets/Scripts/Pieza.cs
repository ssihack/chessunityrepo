﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;


public class Pieza : MonoBehaviour {
     Animator animator;
    Pieza piezaOpuesta;
    protected PiezaVirtual piezaV; 

	// Use this for initialization
    public bool getOwner()
    {
        return piezaV.getOwner();
    }
    public virtual void setOwner(bool owner)
    {
        piezaV.setOwner(owner);
    }
   
    void Awake () {

        Init();
    }
    public virtual void Init()
    {
        piezaV = new PiezaVirtual();
    }
    public virtual TipoPieza getTipo()
    {
        return piezaV.getTipo();
    }

    public virtual PiezaVirtual getPiezaVirtual()
    {
        return piezaV;
    }
	// Update is called once per frame
	void Update () {
		
	}
    public Pieza getPiezaOpuesta()
    {
        return piezaOpuesta;
    }
    public void setPiezaOpuesta(Pieza pieza)
    {
        piezaOpuesta = pieza;
        //ownerPlayer1 = !pieza.ownerPlayer1;
    }
    public void setMatrixIndexes(int x, int y)
    {
        piezaV.setMatrixIndexes(x, y);
    }
    public int[] getMatrixIndexes()
    {
        return piezaV.getMatrixIndexes();
    }

    public bool getPiezaMovida()
    {
        return piezaV.getPiezaMovida();
    }
    public void setPiezaMovida(bool piezaMovida)
    {
        piezaV.setPiezaMovida(piezaMovida);
    }
    public void actualizarMovEspRec()
    {
        piezaV.actualizarMovEspRec();
    }
    public void setTablero(Tablero tablero) {
        piezaV.setTablero(tablero);
    }
}
