﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum DireccionBusqueda
{
    N, NO, NE, E, SE, S, SO, O
}
