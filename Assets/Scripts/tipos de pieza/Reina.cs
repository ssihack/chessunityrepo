﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reina : Pieza {

    public override void Init()
    {
        piezaV = new ReinaV();
    }
    public override void setOwner(bool owner)
    {
        piezaV.setOwner(owner);
    }
}
