﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TorreV : PiezaVirtual {
    private List<int[]> posiblesDestinos = new List<int[]>();

    public TorreV()
    {
        int[] destino = new int[2];
        for(int i = -6; i < 7; i++)
        {
            destino[0] = i; destino[1] = 0;
            posiblesDestinos.Add(destino);
            destino[0] = 0; destino[1] = i;
            posiblesDestinos.Add(destino);
        }        
    }
    public TorreV(int fila, int columna, bool owner) : base(fila, columna, owner)
    {
        int[] destino = new int[2];
        for (int i = -6; i < 7; i++)
        {
            destino[0] = i; destino[1] = 0;
            posiblesDestinos.Add(destino);
            destino[0] = 0; destino[1] = i;
            posiblesDestinos.Add(destino);
        }
    }
    public override PiezaVirtual Clone()
    {
        TorreV clonedTower = new TorreV(this.getPosX(), this.getPosY(), this.owner);
        clonedTower.setTablero(this.tablero);
        return clonedTower;
    }
    public override bool tieneJugadas(TableroV tableroV)
    {
        int[] origen = this.getMatrixIndexes();
        int[] destino = new int[2];
        foreach (int[] coord in posiblesDestinos)
        {
            destino[0] = origen[0] + coord[0];
            destino[1] = origen[1] + coord[1];
            if (comprobarValidezJugada(origen, destino, tableroV))
            {
                return true;
            }
        }
        return false;
    }
    public override TipoPieza getTipo()
    {
        return TipoPieza.TORRE;
    }
    public override bool comprobarValidezJugada(int[] origen, int[] destino, TableroV tableroV)
    {
        PiezaVirtual[,] matrizPiezas = tableroV.getMatrizPiezasV();
        bool filaNoCambia = origen[0] - destino[0] == 0;
        bool columnaNoCambia = origen[1] - destino[1] == 0;
        if (tableroV.HayJaqueEnSiguienteEstado(origen, destino, this))
        {
            return false;
        }
        if (filaNoCambia && columnaNoCambia)
        {
            return false;
        }
        else if(destino[0] > 7 || destino[0] < 0 || destino[1] > 7 || destino[1] < 0)
        {
            return false;
        }
        else if (filaNoCambia)
        {
            return buscarExtremo(origen[0], origen[1], destino[0], destino[1], getOwner(), 0, (destino[1] - origen[1]) / Math.Abs(destino[1] - origen[1]), matrizPiezas);
        }
        else if (columnaNoCambia) {
            return buscarExtremo(origen[0], origen[1], destino[0], destino[1], getOwner(), (destino[0] - origen[0]) / Math.Abs(destino[0] - origen[0]), 0, matrizPiezas);
        }
        else
        {
            return false;
        }
        
    }
    private bool buscarExtremo(int filaOrigen, int columnaOrigen, int filaDestino,int columnaDestino,bool turnoJugador, int vDirI, int vDirJ, PiezaVirtual[,] matrizPiezasV)
    {

        bool condicion1 = true;
        bool condicion2 = true;   
        
        while (condicion1 && condicion2)
        {
            switch (vDirI)
            {
                case -1:
                    condicion1 = filaOrigen >= filaDestino;
                    filaOrigen--;
                    break;
                case 1:
                    condicion1 = filaOrigen <= filaDestino;
                    filaOrigen++;
                    break;
                case 0:
                    condicion1 = true;
                    break;
                default:
                    condicion1 = false;
                    break;
            }
            switch (vDirJ)
            {
                case -1:
                    condicion2 = columnaOrigen >= columnaDestino;
                    columnaOrigen--;
                    break;
                case 1:
                    condicion2 = columnaOrigen <= columnaDestino;
                    columnaOrigen++;
                    break;
                case 0:
                    condicion2 = true;
                    break;
            }
            try//Index Out of Bounds
            {
                PiezaVirtual p = matrizPiezasV[filaOrigen, columnaOrigen];

                if (p != null)//si se ha colocado pieza en esa casilla
                {
                    if (p.getOwner() != turnoJugador && filaOrigen == filaDestino && columnaOrigen == columnaDestino)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if(filaOrigen == filaDestino && columnaOrigen == columnaDestino) //si llegamos hasta aquí, es que no hay ninguna pieza de nuestro mismo color al otro lado de la línea
                {
                    return true;
                }
            }
            catch (IndexOutOfRangeException )
            {
                return false;
            }
        }//si el while no ha devuelto true, mientras las condiciones eran válidas, devuelve false
        return false;
    }
}
