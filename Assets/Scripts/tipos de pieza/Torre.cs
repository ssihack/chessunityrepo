﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : Pieza {

    public override void Init()
    {
        piezaV = new TorreV();
    }
    public override void setOwner(bool owner)
    {

        piezaV.setOwner(owner);
    }

    public override PiezaVirtual getPiezaVirtual()
    {
        return this.piezaV;
    }
}
