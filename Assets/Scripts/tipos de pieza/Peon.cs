﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peon : Pieza {

    public override void Init()
    {
        piezaV = new PeonV();

    }
    
    public override void setOwner(bool owner)
    {
       
        piezaV.setOwner(owner);
    }
    public override TipoPieza getTipo()
    {
        return piezaV.getTipo();
    }

}
