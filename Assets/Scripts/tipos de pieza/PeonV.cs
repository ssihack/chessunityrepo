﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PeonV : PiezaVirtual {
    private List<TuplaCoordenadas> posiblesDestinos = new List<TuplaCoordenadas>();
    public PeonV()
    {
        

    }
    public PeonV(int fila, int columna, bool owner):base(fila,columna,owner)
    {

    }
    private List<TuplaCoordenadas> calculaPosiblesDestinos()
    {
        List<TuplaCoordenadas> posiblesDestinos = new List<TuplaCoordenadas>();
        posiblesDestinos.Add(new TuplaCoordenadas(1, 0));
        posiblesDestinos.Add(new TuplaCoordenadas(-1, 0));
        posiblesDestinos.Add(new TuplaCoordenadas(2, 0));
        posiblesDestinos.Add(new TuplaCoordenadas(-2, 0));
        posiblesDestinos.Add(new TuplaCoordenadas(1, 1));
        posiblesDestinos.Add(new TuplaCoordenadas(-1, -1));
        posiblesDestinos.Add(new TuplaCoordenadas(1, -1));
        posiblesDestinos.Add(new TuplaCoordenadas(-1, 1));
        
        
        return posiblesDestinos;
    }

    public override bool tieneJugadas(TableroV tableroV)
    {
        posiblesDestinos = calculaPosiblesDestinos();
        int[] origen = this.getMatrixIndexes();
        int[] destino = new int[2];
        foreach (TuplaCoordenadas coord in posiblesDestinos)
        {
            destino[0] = origen[0] + coord.getX();
            destino[1] = origen[1] + coord.getY();
            if(destino[0] < 0 || destino[0] > 7 || destino[1] < 0 || destino[1] > 7)
            {
                continue;
            }
            if (comprobarValidezJugada(origen, destino, tableroV))
            {
                return true;
            }
        }
        return false;
    }
    public override PiezaVirtual Clone()
    {
        PeonV peonClonado = new PeonV(this.getPosX(), this.getPosY(), this.getOwner());
        peonClonado.setTablero(this.tablero);
        return peonClonado;
    }
    public override TipoPieza getTipo()//new es lo mismo que @overwrite de java
    {
        return TipoPieza.PEON;
    }
    bool casillaEstaOcupadaPorElMismoJugadorQueMueve(PiezaVirtual piezaMovida, PiezaVirtual piezaEnDestino)
    {
        return piezaEnDestino != null && piezaMovida != null && piezaEnDestino.getOwner() == piezaMovida.getOwner();
    }
    
    public override bool comprobarValidezJugada(int[] origen, int[] destino, TableroV tableroV)
    {
        if (tableroV.HayJaqueEnSiguienteEstado(origen, destino, this))
        {
            return false;
        }
        PiezaVirtual[,] matrizPiezas = tableroV.getMatrizPiezasV();
        if (destino[1] < 0 || destino[1] > 7)
        {
            return false;
        }
        PiezaVirtual piezaEnDestino = matrizPiezas[destino[0], destino[1]];       
        if (casillaEstaOcupadaPorElMismoJugadorQueMueve(this,piezaEnDestino))
        {
            return false;
        }
        else
        {
            // Debug.Log("ultimo evaluado en: " +origen[0] + ", " + origen[1]);//parece que se ejecuta varias veces
            
            if (matrizPiezas[origen[0], origen[1]]!= null && matrizPiezas[origen[0], origen[1]].getOwner())//si es el jugador 1
            {
                if(origen[0] -1 == destino[0] && origen[1] == destino[1] && piezaEnDestino ==null)//si la casilla está justo delante
                { //mover
                    piezaMovida = true;
                   
                    return true;
                }
                else if (origen[0] - 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && piezaEnDestino != null)
                    //comer
                {
                    piezaMovida = true;
                    
                    return true;
                }
                else if (origen[0] - 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && matrizPiezas[destino[0]+1,destino[1]] != null && matrizPiezas[destino[0] + 1, destino[1]].isMovEspReciente())
                //comer al paso
                {

                    Tablero.getTablero().setPiezaCapturadaAlPaso(destino[0] + 1, destino[1]);//es +1, porque está delante de la casilla a la que voy a acceder
                    piezaMovida = true;
                    return true;
                }
                else if (!this.piezaMovida && origen[0] - 2 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)//salta por encima de las piezas
                { //avanzar dos casillas al salir
                    
                    if(buscarExtremo(origen[0], origen[1], destino[0], destino[1], true, -1, 0, matrizPiezas))
                    {
                        this.piezaMovida = true;
                        this.movimientoEspecialReciente = true;
                        tablero.comunicarMovEspecialReciente(this);
                        return true;
                    }
                }
                return false;
            }
            else//si es el jugador 2
            {
                if (origen[0] + 1 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)//si la casilla está justo delante
                {
                   
                    piezaMovida = true;
                    return true;
                }
                else if (origen[0] + 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && piezaEnDestino != null)
                //comer
                {
                  
                    piezaMovida = true;
                    return true;
                }
                else if (origen[0] + 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && matrizPiezas[destino[0] - 1, destino[1]] != null && matrizPiezas[destino[0] - 1, destino[1]].isMovEspReciente())
                //comer al paso
                {
                  
                    Tablero.getTablero().setPiezaCapturadaAlPaso(destino[0] - 1, destino[1]);
                    piezaMovida = true;
                    return true;
                }
                else if (!piezaMovida && origen[0] + 2 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)
                {//mover 2 casillas al salir
                   
                    piezaMovida = true;
                    this.movimientoEspecialReciente = true;
                    tablero.comunicarMovEspecialReciente(this);
                    return true;
                }
                return false;
            }
        }
        
    }
    public override bool comprobarValidezJugada(int[] origen, int[] destino, PiezaVirtual[,] matrizPiezas, ClickListener[,] matrizTiles)
    {//Sin llamadas BORRAR
        PiezaVirtual piezaEnDestino = matrizPiezas[destino[0], destino[1]];

        if (piezaEnDestino != null && piezaEnDestino.getOwner() == matrizPiezas[origen[0], origen[1]].getOwner())
        {//si la casilla está ocupada por el mismo jugador q mueve:
            return false;
        }
        else
        {
            if (matrizPiezas[origen[0], origen[1]].getOwner())//si es el jugador 1
            {
                if (origen[0] - 1 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)//si la casilla está justo delante
                { //mover
                    piezaMovida = true;
                    return true;
                }
                else if (origen[0] - 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && piezaEnDestino != null)
                //comer
                {
                    piezaMovida = true;
                    return true;
                }
                else if (origen[0] - 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && matrizPiezas[destino[0] + 1, destino[1]] != null /*&& matrizPiezas[destino[0] + 1, destino[1]].isMovEspReciente()*/)
                //comer al paso
                {
                    //tablero.setPiezaCapturadaAlPaso(destino[0] + 1, destino[1]);//es +1, porque está delante de la casilla a la que voy a acceder
                    matrizTiles[destino[0] + 1, destino[1]].retirarPiezaDeCasilla();
                    piezaMovida = true;
                    return true;
                }
                else if (!this.piezaMovida && origen[0] - 2 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)//salta por encima de las piezas
                { //avanzar dos casillas al salir

                    if (buscarExtremo(origen[0], origen[1], destino[0], destino[1], true, -1, 0, matrizPiezas))
                    {
                        this.piezaMovida = true;
                        this.movimientoEspecialReciente = true;
                        return true;
                    }
                }
                return false;
            }
            else//si es el jugador 2
            {
                if (origen[0] + 1 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)//si la casilla está justo delante
                {
                    piezaMovida = true;
                    return true;
                }
                else if (origen[0] + 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && piezaEnDestino != null)
                //comer
                {
                    piezaMovida = true;
                    return true;
                }
                else if (origen[0] + 1 == destino[0] && (origen[1] - 1 == destino[1] || origen[1] + 1 == destino[1]) && matrizPiezas[destino[0] - 1, destino[1]] != null && matrizPiezas[destino[0] - 1, destino[1]].isMovEspReciente())
                //comer al paso
                {
                    tablero.setPiezaCapturadaAlPaso(destino[0] - 1, destino[1]);
                    piezaMovida = true;
                    return true;
                }
                else if (!piezaMovida && origen[0] + 2 == destino[0] && origen[1] == destino[1] && piezaEnDestino == null)
                {
                    piezaMovida = true;
                    this.movimientoEspecialReciente = true;
                    return true;
                }
                return false;
            }
        }

    }

    private bool buscarExtremo(int filaOrigen, int columnaOrigen, int filaDestino, int columnaDestino, bool turnoJugador, int vDirI, int vDirJ, PiezaVirtual[,] matrizPiezasV)
    {

        bool condicion1 = true;
        bool condicion2 = true;

        while (condicion1 && condicion2)
        {
            switch (vDirI)
            {
                case -1:
                    condicion1 = filaOrigen >= filaDestino;
                    filaOrigen--;
                    break;
                case 1:
                    condicion1 = filaOrigen <= filaDestino;
                    filaOrigen++;
                    break;
                case 0:
                    condicion1 = true;
                    break;
                default:
                    condicion1 = false;
                    break;
            }
            switch (vDirJ)
            {
                case -1:
                    condicion2 = columnaOrigen >= columnaDestino;
                    columnaOrigen--;
                    break;
                case 1:
                    condicion2 = columnaOrigen <= columnaDestino;
                    columnaOrigen++;
                    break;
                case 0:
                    condicion2 = true;
                    break;
            }
            try//Index Out of Bounds
            {
                PiezaVirtual p = matrizPiezasV[filaOrigen, columnaOrigen];

                if (p != null)//si se ha colocado pieza en esa casilla
                {
                    if (p.getOwner() != turnoJugador && filaOrigen == filaDestino && columnaOrigen == columnaDestino)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (filaOrigen == filaDestino && columnaOrigen == columnaDestino) //si llegamos hasta aquí, es que no hay ninguna pieza de nuestro mismo color al otro lado de la línea
                {
                    return true;
                }
            }
            catch (IndexOutOfRangeException )
            {
                return false;
            }
        }//si el while no ha devuelto true, mientras las condiciones eran válidas, devuelve false
        return false;
    }
    
}
