﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReyV : PiezaVirtual
{
    private List<TuplaCoordenadas> posiblesDestinos = new List<TuplaCoordenadas>();

    public ReyV()
    {
        
    }
    public ReyV(int posX, int posY, bool owner) : base(posX, posY, owner)
    {

    }
    public override PiezaVirtual Clone()
    {
        ReyV clonedKing = new ReyV(this.getPosX(), this.getPosY(), this.getOwner());
        clonedKing.setTablero(this.tablero);
        return clonedKing;
    }
    private List<TuplaCoordenadas> calculaPosiblesDestinos()
    {
        List<TuplaCoordenadas> posiblesDestinos = new List<TuplaCoordenadas>();
            posiblesDestinos.Add(new TuplaCoordenadas(1,1));
            posiblesDestinos.Add(new TuplaCoordenadas(-1, 1));
            posiblesDestinos.Add(new TuplaCoordenadas(1, 0));
            posiblesDestinos.Add(new TuplaCoordenadas(0, 1));
        posiblesDestinos.Add(new TuplaCoordenadas(-1, 0));
        posiblesDestinos.Add(new TuplaCoordenadas(0, -1));
        posiblesDestinos.Add(new TuplaCoordenadas(1, -1));
            posiblesDestinos.Add(new TuplaCoordenadas(-1, -1));
        return posiblesDestinos;
    }
    public override bool tieneJugadas(TableroV tableroV)
    {
        posiblesDestinos = calculaPosiblesDestinos();
        int[] origen = this.getMatrixIndexes();
        int[] destino = new int[2];
        foreach (TuplaCoordenadas coord in posiblesDestinos)
        {
            destino[0] = origen[0] + coord.getX();
            destino[1] = origen[1] + coord.getY();
            if(destino[0] < 0 || destino[0] > 7 || destino[1] < 0 || destino[1] > 7)
            {
                continue;
            }
            if (comprobarValidezJugada(origen, destino, tableroV))
            {
                return true;
            }
        }
        return false;
    }
    public override TipoPieza getTipo()//new es lo mismo que @overwrite de java
    {
        return TipoPieza.REY;
    }

   

    public override bool comprobarValidezJugada(int[] origen, int[] destino, TableroV tableroV)
    {
        PiezaVirtual[,] matrizPiezas = tableroV.getMatrizPiezasV();
        PiezaVirtual piezaEnDestino = matrizPiezas[destino[0], destino[1]];

        if (casillaOcupadaPorElMismoJugadorQueMueve(origen,destino,tableroV))
        {
            return false;
        }
        else
        {
            if (!tableroV.IsCasillaAmenazada(this.getOwner(), this, destino[0], destino[1])) {
                

                    if (isEnroqueValido(origen, destino, tableroV))
                    {
                        return true;
                        
                    }
                    else if(isCasillaAdyacente(origen, destino))
                    {                                                
                        Debug.Log("coordenadas destino: " + destino[0] + " , " + destino[1]);
                        piezaMovida = true;
                        return true;
                    }

                }
            Debug.Log("es casilla amenazada: " + tableroV.IsCasillaAmenazada(this.getOwner(), destino[0], destino[1]));
            return false;
        }

    }
    private bool isEnroqueValido(int[] origen, int[] destino, TableroV tableroV)
    {
        if (tableroV.isDestinoADosCasillasDeDistancia(origen[1], destino[1]) && !tableroV.getMatrizPiezasV()[origen[0], origen[1]].getPiezaMovida())
        {
            if(isEnroqueCorto(origen[1],destino[1]))
            {
                if(tableroV.getMatrizPiezasV()[origen[0],7] != null && tableroV.getMatrizPiezasV()[origen[0], 7].getTipo().Equals(TipoPieza.TORRE) && !tableroV.getMatrizPiezasV()[origen[0], 7].getPiezaMovida())
                {
                    if(tableroV.getMatrizPiezasV()[origen[0], 6] == null && tableroV.getMatrizPiezasV()[origen[0], 5] == null && !tableroV.IsCasillaAmenazada(getOwner(), origen[0],7) 
                        && !tableroV.IsCasillaAmenazada(getOwner(), origen[0], 6) && !tableroV.IsCasillaAmenazada(getOwner(), origen[0], 5))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                if (tableroV.getMatrizPiezasV()[origen[0], 0] != null && tableroV.getMatrizPiezasV()[origen[0], 0].getTipo().Equals(TipoPieza.TORRE) && !tableroV.getMatrizPiezasV()[origen[0], 0].getPiezaMovida())
                {
                    if (tableroV.getMatrizPiezasV()[origen[0], 1] == null && tableroV.getMatrizPiezasV()[origen[0], 2] == null && tableroV.getMatrizPiezasV()[origen[0], 3] == null && !tableroV.IsCasillaAmenazada(getOwner(), origen[0], 0)
                        && !tableroV.IsCasillaAmenazada(getOwner(), origen[0], 1) && !tableroV.IsCasillaAmenazada(getOwner(), origen[0], 2) && !tableroV.IsCasillaAmenazada(getOwner(), origen[0], 3))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }
  
    

    private bool isEnroqueCorto(int columnaOrigen, int columnaDestino)
    {
        return columnaDestino > columnaOrigen;
    }
    private bool isCasillaAdyacente(int[] origen, int[] destino)
    {
        return (Mathf.Abs(origen[0] - destino[0]) <= 1 && Mathf.Abs(origen[1] - destino[1]) <= 1);
    }
   
}
