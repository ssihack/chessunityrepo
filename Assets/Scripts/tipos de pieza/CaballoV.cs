﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaballoV : PiezaVirtual {
    private List<CasillaTablero> zonaDeControl;
    private List<int[]> posiblesDestinos = new List<int[]>();
     public override TipoPieza getTipo()
    {
        return TipoPieza.CABALLO;
    }
    public CaballoV()
    {
        int[] destino = new int[2];
        destino[0] = -2; destino[1] = -1;
        posiblesDestinos.Add(destino);
        destino[0] = -2; destino[1] = 1;
        posiblesDestinos.Add(destino);
        destino[0] = 2; destino[1] = -1;
        posiblesDestinos.Add(destino);
        destino[0] = 2; destino[1] = 1;
        posiblesDestinos.Add(destino);
        destino[0] = -1; destino[1] = -2;
        posiblesDestinos.Add(destino);
        destino[0] = -1; destino[1] = 2;
        posiblesDestinos.Add(destino);
        destino[0] = 1; destino[1] = -2;
        posiblesDestinos.Add(destino);
        destino[0] = 1; destino[1] = 2;
        posiblesDestinos.Add(destino);
        
    }
    public CaballoV(int posX, int posY, bool owner) : base(posX, posY, owner)
    {
        int[] destino = new int[2];
        destino[0] = -2; destino[1] = -1;
        posiblesDestinos.Add(destino);
        destino[0] = -2; destino[1] = 1;
        posiblesDestinos.Add(destino);
        destino[0] = 2; destino[1] = -1;
        posiblesDestinos.Add(destino);
        destino[0] = 2; destino[1] = 1;
        posiblesDestinos.Add(destino);
        destino[0] = -1; destino[1] = -2;
        posiblesDestinos.Add(destino);
        destino[0] = -1; destino[1] = 2;
        posiblesDestinos.Add(destino);
        destino[0] = 1; destino[1] = -2;
        posiblesDestinos.Add(destino);
        destino[0] = 1; destino[1] = 2;
        posiblesDestinos.Add(destino);
    }
    public override PiezaVirtual Clone()
    {
        CaballoV clonedKnight = new CaballoV(this.getPosX(), this.getPosY(), this.getOwner());
        clonedKnight.setTablero(this.tablero);
        return clonedKnight;
    }
    public override bool comprobarValidezJugada(int[] origen, int[] destino, TableroV tableroV)
    {
        
        if (tableroV.HayJaqueEnSiguienteEstado(origen, destino, this))
        {
            return false;
        }
        PiezaVirtual[,] matrizPiezas = tableroV.getMatrizPiezasV();
        if (destino[1] < 0 || destino[1] > 7 || destino[0] < 0 || destino[0] > 7)
        {
            return false;
        }
        PiezaVirtual piezaEnDestino = matrizPiezas[destino[0], destino[1]];
        if (piezaEnDestino != null && piezaEnDestino.getOwner() == matrizPiezas[origen[0], origen[1]].getOwner())
        {//si la casilla está ocupada por el mismo jugador q mueve:
            return false;
        }
        else
        {
            if ((origen[0] - 2 == destino[0] || origen[0] + 2 == destino[0]) && (origen[1] + 1 == destino[1] || origen[1] - 1 == destino[1]))
            {
                return true;
            }
            else if ((origen[0] - 1 == destino[0] || origen[0] + 1 == destino[0]) && (origen[1] + 2 == destino[1] || origen[1] - 2 == destino[1]))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
    }
    public override bool tieneJugadas(TableroV tableroV)
    {
        int[] origen = this.getMatrixIndexes();
        int[] destino = new int [ 2 ];
        foreach(int[] coord in posiblesDestinos)
        {
            destino[0] = origen[0] + coord[0];
            destino[1] = origen[1] + coord[1];
            if (comprobarValidezJugada(origen, destino, tableroV))
            {
                return true;
            }
        }
        return false;
    }
    public override void actualizarZonaDeControl(CasillaTablero[,] matrizCasillas, PiezaVirtual p)
    {
        foreach(CasillaTablero ca in zonaDeControl)
        {
            ca.retirarControlDePieza(p, p.getOwner());            
        }
        zonaDeControl = new List<CasillaTablero>();
        int []posicionPieza = this.getMatrixIndexes();
        CasillaTablero c = matrizCasillas[posicionPieza[0] - 1, posicionPieza[1] - 2];
        c.añadirControlDePieza(p,p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] - 1, posicionPieza[1] + 2];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] + 1, posicionPieza[1] - 2];
            c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);

        c = matrizCasillas[posicionPieza[0] + 1, posicionPieza[1] + 2];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);

        c = matrizCasillas[posicionPieza[0] - 2, posicionPieza[1] - 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] - 2, posicionPieza[1] + 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] + 2, posicionPieza[1] - 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] + 2, posicionPieza[1] + 1];
            c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
    }
}
