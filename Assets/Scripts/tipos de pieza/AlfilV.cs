﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class AlfilV : PiezaVirtual {
    private List<CasillaTablero> zonaDeControl;
    private List<int[]> posiblesDestinos = new List<int[]>();

    public AlfilV()
    {
        int[] destino = new int[2];
        for (int i = -6; i < 7; i++)
        {
            destino[0] = i; destino[1] = i;
            posiblesDestinos.Add(destino);
            destino[0] = -i; destino[1] = i;
            posiblesDestinos.Add(destino);
        }
    }
    public AlfilV(int fila, int columna, bool owner):base(fila,columna,owner)
    {
        int[] destino = new int[2];
        for (int i = -6; i < 7; i++)
        {
            destino[0] = i; destino[1] = i;
            posiblesDestinos.Add(destino);
            destino[0] = -i; destino[1] = i;
            posiblesDestinos.Add(destino);
        }
    }
    public override PiezaVirtual Clone()
    {
        AlfilV clonedBishop = new AlfilV(this.getPosX(), this.getPosY(), this.getOwner());
        clonedBishop.setTablero(this.tablero);
        return clonedBishop;
    }
    public override bool tieneJugadas(TableroV tableroV)
    {
        int[] origen = this.getMatrixIndexes();
        int[] destino = new int[2];
        foreach (int[] coord in posiblesDestinos)
        {
            destino[0] = origen[0] + coord[0];
            destino[1] = origen[1] + coord[1];
            if (comprobarValidezJugada(origen, destino, tableroV))
            {
                return true;
            }
        }
        return false;
    }
    public override TipoPieza getTipo()//new es lo mismo que @overwrite de java
    {
        return TipoPieza.ALFIL;
    }

    public override bool comprobarValidezJugada(int[] origen, int[] destino, TableroV tableroV)
    {
        PiezaVirtual[,] matrizPiezas = tableroV.getMatrizPiezasV();
        bool filaNoCambia = origen[0] - destino[0] == 0;
        bool columnaNoCambia = origen[1] - destino[1] == 0;
        if (tableroV.HayJaqueEnSiguienteEstado(origen, destino, this))
        {
            return false;
        }
        if (filaNoCambia || columnaNoCambia)
        {
            return false;
        }
        else if (destino[0] > 7 || destino[0] < 0 || destino[1] > 7 || destino[1] < 0)
        {
            return false;
        }
        else if (Math.Abs(destino[0]-origen[0])!= Math.Abs(destino[1] - origen[1]))//si la casilla destino no se encuentra en la misma diagonal que la origen:
        {
            return false;
        }       
        else
        {
            return buscarExtremo(origen[0], origen[1], destino[0], destino[1], getOwner(), (destino[0] - origen[0]) / Math.Abs(destino[0] - origen[0]), (destino[1] - origen[1]) / Math.Abs(destino[1] - origen[1]), matrizPiezas);
        }

    }
    private bool buscarExtremo(int filaOrigen, int columnaOrigen, int filaDestino, int columnaDestino, bool turnoJugador, int vDirI, int vDirJ, PiezaVirtual[,] matrizPiezasV)
    {

        bool condicion1 = true;
        bool condicion2 = true;

        while (condicion1 && condicion2)
        {
            switch (vDirI)
            {
                case -1:
                    condicion1 = filaOrigen >= filaDestino;
                    filaOrigen--;
                    break;
                case 1:
                    condicion1 = filaOrigen <= filaDestino;
                    filaOrigen++;
                    break;
                case 0:
                    condicion1 = true;
                    break;
                default:
                    condicion1 = false;
                    break;
            }
            switch (vDirJ)
            {
                case -1:
                    condicion2 = columnaOrigen >= columnaDestino;
                    columnaOrigen--;
                    break;
                case 1:
                    condicion2 = columnaOrigen <= columnaDestino;
                    columnaOrigen++;
                    break;
                case 0:
                    condicion2 = true;
                    break;
            }
            try//Index Out of Bounds
            {
                PiezaVirtual p = matrizPiezasV[filaOrigen, columnaOrigen];

                if (p != null)//si se ha colocado pieza en esa casilla
                {
                    if (p.getOwner() != turnoJugador && filaOrigen == filaDestino && columnaOrigen == columnaDestino)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (filaOrigen == filaDestino && columnaOrigen == columnaDestino) //si llegamos hasta aquí, es que no hay ninguna pieza de nuestro mismo color al otro lado de la línea
                {
                    return true;
                }
            }
            catch (IndexOutOfRangeException )
            {
                return false;
            }
        }//si el while no ha devuelto true, mientras las condiciones eran válidas, devuelve false
        return false;
    }
    public override void actualizarZonaDeControl(CasillaTablero[,] matrizCasillas, PiezaVirtual p)
    {
        foreach (CasillaTablero ca in zonaDeControl)
        {
            ca.retirarControlDePieza(p, p.getOwner());
        }
        zonaDeControl = new List<CasillaTablero>();
        int[] posicionPieza = this.getMatrixIndexes();
        CasillaTablero c;
        int desplazamientoFila, desplazamientoColumna;
        for (int i = 1; i < 6; i++)
        {
            desplazamientoFila = posicionPieza[0] + i;
            desplazamientoColumna = posicionPieza[1] - i;
            if (!(desplazamientoFila > 7  || desplazamientoColumna > 7))
            {
                c = matrizCasillas[posicionPieza[0] + i, posicionPieza[1] + i];
                zonaDeControl.Add(c);
                if(c.getPieza() != null)//aquí se puede detectar al rey
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        for (int i = 1; i < 6; i++)
        {
            desplazamientoFila = posicionPieza[0] + i;
            desplazamientoColumna = posicionPieza[1] - i;
            if (!(desplazamientoFila > 7 || desplazamientoColumna > 7))
            {
                c = matrizCasillas[posicionPieza[0] + i, posicionPieza[1] + i];
                zonaDeControl.Add(c);
                if (c.getPieza() != null)//aquí se puede detectar al rey
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        /*
            desplazamientoColumna = posicionPieza[1] - i;
            if(desplazamientoColumna < 0 || desplazamientoColumna > 7)
            {
                c = matrizCasillas[posicionPieza[0] + i, posicionPieza[1] + i];
            }
            
        }*/
        c = matrizCasillas[posicionPieza[0] - 1, posicionPieza[1] - 2];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] - 1, posicionPieza[1] + 2];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] + 1, posicionPieza[1] - 2];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);

        c = matrizCasillas[posicionPieza[0] + 1, posicionPieza[1] + 2];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);

        c = matrizCasillas[posicionPieza[0] - 2, posicionPieza[1] - 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] - 2, posicionPieza[1] + 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] + 2, posicionPieza[1] - 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
        c = matrizCasillas[posicionPieza[0] + 2, posicionPieza[1] + 1];
        c.añadirControlDePieza(p, p.getOwner());
        zonaDeControl.Add(c);
    }
}
