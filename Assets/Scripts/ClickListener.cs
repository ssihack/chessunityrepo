﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickListener : MonoBehaviour
{

    // Use this for initialization
    public GameObject peonBlanco;
    public GameObject torreBlanca;
    public GameObject caballoBlanco;
    public GameObject alfilBlanco;
    public GameObject reinaBlanca;
    public GameObject reyBlanco;
    public GameObject peonNegro;
    public GameObject torreNegro;
    public GameObject caballoNegro;
    public GameObject alfilNegro;
    public GameObject reinaNegro;
    public GameObject reyNegro;
    //public GameObject piezaNegra;
    //cuando consiga rotar las piezas, las siguientes dos variables se convertirán en una
    private GameObject pieza;
    //public enum TipoPieza { REY, DAMA, ALFIL, CABALLO, TORRE, PEON};
    private Pieza piezaColocada;
    //
    private int[] casillaInicial;
    private int fila, columna;
    private Tablero tablero;
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetFilaYColumna(int fila, int columna)
    {
        this.fila = fila;
        this.columna = columna;
    }
    void OnMouseDown()
    {
      Debug.Log("click recogido, en fila: " + fila + "columna: " + columna);
        
        
        tablero.comprobarValidezClickJugada(this, this.fila, this.columna);
    }

    public int getFila()
    {
        return fila;
    }
    public int getColumna()
    {
        return columna;
    }
    
    public void setTablero(Tablero tablero)
    {
        this.tablero = tablero;
    }
    public Pieza colocarPieza(bool player,TipoPieza tipo)
    {
        if (player)//si player es player 1
        {
            switch (tipo)
            {
                case TipoPieza.DAMA:
                    pieza = Instantiate(reinaBlanca);
                    break;
                case TipoPieza.REY:
                    pieza = Instantiate(reyBlanco);
                    break;
                case TipoPieza.ALFIL:
                    pieza = Instantiate(alfilBlanco);
                    break;
                case TipoPieza.CABALLO:
                    pieza = Instantiate(caballoBlanco);
                    break;
                case TipoPieza.TORRE:
                    pieza = Instantiate(torreBlanca);
                    break;
                case TipoPieza.PEON:
                    pieza = Instantiate(peonBlanco);
                    break;
            }
        }
        else
        {
            switch (tipo)
            {
                case TipoPieza.DAMA:
                    pieza = Instantiate(reinaNegro);
                    break;
                case TipoPieza.REY:
                    pieza = Instantiate(reyNegro);
                    break;
                case TipoPieza.ALFIL:
                    pieza = Instantiate(alfilNegro);
                    break;
                case TipoPieza.CABALLO:
                    pieza = Instantiate(caballoNegro);
                    break;
                case TipoPieza.TORRE:
                    pieza = Instantiate(torreNegro);
                    break;
                case TipoPieza.PEON:
                    pieza = Instantiate(peonNegro);
                    break;
            }
        }
        //pieza.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
        
        pieza.transform.position = transform.position;
        pieza.transform.position = transform.position + new Vector3(0, 1, 0) * pieza.GetComponent<MeshRenderer>().bounds.size.y/2;
        
        Pieza p = pieza.GetComponent<Pieza>();
        
        if (p)
        {
            p.setOwner(player);
            p.setMatrixIndexes(fila, columna);
            piezaColocada = p;
            return p;
        }
       throw new System.Exception();


    }
    public Pieza colocarPieza(bool player, TipoPieza tipo, bool piezaMovida)
    {
        Pieza p = colocarPieza(player, tipo);
        p.setPiezaMovida(piezaMovida);
        return p;
    }
    public Pieza getPiezaColocada()
    {
        return piezaColocada;
    }
    public void retirarPiezaDeCasilla()
    {
        if(piezaColocada)
        {
            Destroy(piezaColocada.gameObject);
            piezaColocada = null;
        }
    }
}
