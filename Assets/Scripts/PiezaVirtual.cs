﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiezaVirtual {

	private int posX, posY;
	protected bool owner;
    protected bool piezaMovida;
    protected bool movimientoEspecialReciente;
    protected Tablero tablero;
	public PiezaVirtual(int posX, int posY, bool owner){
		this.posX = posX;
		this.posY = posY;
		this.owner = owner;
	}
    public PiezaVirtual(int posX, int posY)
    {
        this.posX = posX;
        this.posY = posY;
        
    }
    public void setTablero(Tablero tablero)//sustituir por Estado s
    {
        this.tablero = tablero;
    }
    public bool isMovEspReciente() {
        return movimientoEspecialReciente;
    }
    public PiezaVirtual(){
		
	}
    public void actualizarMovEspRec()
    {
        movimientoEspecialReciente = false;
    }
    public virtual TipoPieza getTipo()
    {
        return TipoPieza.NONE;
    }
    public bool getOwner(){
		return owner;
	}
	public void setOwner(bool owner)
	{
		this.owner = owner;
	}
	public void setMatrixIndexes(int x, int y)
	{
		posX = x;
		posY = y;
	}
	public int[] getMatrixIndexes()
	{
		int[] indexes = new int[2];
		indexes[0] = posX;
		indexes[1] = posY;
		return indexes;
	}
    public int getPosX()
    {
        return posX;
    }
    public int getPosY()
    {
        return posY;
    }
    public virtual PiezaVirtual Clone(){
		PiezaVirtual newPiece = new PiezaVirtual (this.posX,this.posY,this.owner);
        newPiece.setTablero(this.tablero);
		return newPiece;
	}

 

    public override int GetHashCode()
    {
        //float pendienteVPos = posY / (posX + 1);//esto evita falsas equivalencias cuando x1 = y2, y x2 = y1; Se suma 1 para no dividir entre 0
        var hashCode = -1754300297;
        hashCode = hashCode * -1521134295 + (23*posX.GetHashCode());
        hashCode = hashCode * -1521134295 + posY.GetHashCode();
        //hashCode = hashCode * -1521134295 + pendienteVPos.GetHashCode();
        hashCode = hashCode * -1521134295 + owner.GetHashCode();
        return hashCode;
    }

    public override bool Equals(object obj)
    {
        var @virtual = obj as PiezaVirtual;
        return @virtual != null &&
               posX == @virtual.posX &&
               posY == @virtual.posY &&
               owner == @virtual.owner;
    }
    public virtual bool comprobarValidezJugada(int[] origen, int[] destino, TableroV tableroV)
    {
        return false;
    }
    public virtual bool comprobarValidezJugada(int[] origen, int[] destino, PiezaVirtual[,] matrizPiezas, ClickListener[,]matrizTiles)
    {
        return false;
    }
    public bool getPiezaMovida()
    {
        return piezaMovida;
    }
    public void setPiezaMovida(bool piezaMovida)
    {
        this.piezaMovida = piezaMovida;
    }
    public virtual void actualizarZonaDeControl(CasillaTablero[,] matrizCasillas, PiezaVirtual p)
    {

    }
    public virtual bool tieneJugadas(TableroV tableroV)
    {
        return false;
    }
    public bool casillaOcupadaPorElMismoJugadorQueMueve(int[] origen, int[] destino, TableroV tableroV)
    {
        PiezaVirtual[,] matrizPiezas = tableroV.getMatrizPiezasV();
        PiezaVirtual piezaEnDestino = matrizPiezas[destino[0], destino[1]];
        return piezaEnDestino != null && matrizPiezas[origen[0], origen[1]] != null && piezaEnDestino.getOwner() == matrizPiezas[origen[0], origen[1]].getOwner();
    }
}
