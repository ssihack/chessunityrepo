﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class TableroTest {
    private TableroExtended tablero;
    [Test]
	public void TableroTestSimplePasses() {
		// Use the Assert class to test conditions.
	}

	// A UnityTest behaves like a coroutine in PlayMode
	// and allows you to yield null to skip a frame in EditMode
	[UnityTest]
	public IEnumerator TableroTestWithEnumeratorPasses() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}

    [Test]
    public void GivenWhitePawnAndGivenAnyInmediateDiagonalTileWithRowNumberHigherThanPawnsRowNumberWhenInvockedIsReacheableByEnemyPawnThenReturnsTrue()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 1;
        tablero.addWhitePawn(row, column);
        Assert.True(tablero.isReachableByEnemyPawn(false, row + 1, column + 1));
        Assert.True(tablero.isReachableByEnemyPawn(false, row + 1, column - 1));
    }

    [Test]
    public void GivenWhitePawnAtAnyHorizontalBorderAndGivenAnyInmediateDiagonalTileWithRowNumberLowerThanPawnsRowNumberWhenInvockedIsReacheableByEnemyPawnThenReturnsTrue()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 0;
        tablero.addWhitePawn(row, column);
        Assert.True(tablero.isReachableByEnemyPawn(false, row + 1, column + 1));
        column = 7;
        tablero.addWhitePawn(row, column);
        Assert.True(tablero.isReachableByEnemyPawn(false, row + 1, column - 1));
    }

    [Test]
    public void GivenWhitePawnAndGivenAnyInmediateDiagonalTileWithRowNumberLowerThanPawnsRowNumberWhenInvockedIsReacheableByEnemyPawnThenReturnsFalse()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 1;
        tablero.addWhitePawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(false, row - 1, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row - 1, column + 1));
    }

    [Test]
    public void GivenAnyWhitePawnAndGivenAnyInmediateHorizontalOrVerticalRowWhenInvockedIsReachableByEnemyPawnThenReturnsFalse()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 5;
        tablero.addWhitePawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(true, row, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(true, row, column + 1));
        Assert.False(tablero.isReachableByEnemyPawn(true, row +1, column));
        Assert.False(tablero.isReachableByEnemyPawn(true, row -1, column));
        column = 0;
        tablero.addWhitePawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(true, row, column + 1));
        Assert.False(tablero.isReachableByEnemyPawn(true, row + 1, column));
        Assert.False(tablero.isReachableByEnemyPawn(true, row - 1, column));
        column = 7;
        tablero.addWhitePawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(true, row, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(true, row + 1, column));
        Assert.False(tablero.isReachableByEnemyPawn(true, row - 1, column));
    }

    //
    [Test]
    public void GivenBlackPawnAndGivenAnyInmediateDiagonalTileWithRowNumberLowerThanPawnsRowNumberWhenInvockedIsReacheableByEnemyPawnThenReturnsTrue()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 1;
        tablero.addBlackPawn(row, column);
        Assert.True(tablero.isReachableByEnemyPawn(true, row - 1, column + 1));
        Assert.True(tablero.isReachableByEnemyPawn(true, row - 1, column - 1));
    }

    [Test]
    public void GivenBlackPawnAtAnyHorizontalBorderAndGivenAnyInmediateDiagonalTileWithRowNumberLowerThanPawnsRowNumberWhenInvockedIsReacheableByEnemyPawnThenReturnsTrue()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 0;
        tablero.addBlackPawn(row, column);
        Assert.True(tablero.isReachableByEnemyPawn(true, row - 1, column + 1));
        column = 7;
        tablero.addBlackPawn(row, column);
        Assert.True(tablero.isReachableByEnemyPawn(true, row - 1, column - 1));
    }

    [Test]
    public void GivenBlackPawnAndGivenAnyInmediateDiagonalTileWithRowNumberHigherThanPawnsRowNumberWhenInvockedIsReacheableByEnemyPawnThenReturnsFalse()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 1;
        tablero.addBlackPawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(false, row + 1, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row + 1, column + 1));
    }

    [Test]
    public void GivenAnyBlackPawnAndGivenAnyInmediateHorizontalOrVerticalRowWhenInvockedIsReachableByEnemyPawnThenReturnsFalse()
    {
        TableroExtended tablero = new TableroExtended();
        int row = 2;
        int column = 5;
        tablero.addBlackPawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(false, row, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row, column + 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row + 1, column));
        Assert.False(tablero.isReachableByEnemyPawn(false, row - 1, column));
        column = 0;
        tablero.addBlackPawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(false, row, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row, column + 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row + 1, column));
        Assert.False(tablero.isReachableByEnemyPawn(false, row - 1, column));
        column = 7;
        tablero.addBlackPawn(row, column);
        Assert.False(tablero.isReachableByEnemyPawn(false, row, column - 1));
        Assert.False(tablero.isReachableByEnemyPawn(false, row + 1, column));
        Assert.False(tablero.isReachableByEnemyPawn(false, row - 1, column));
    }
    [Test]
    public void GivenAnyWhiteTowerAndGivenAnyTileInSameRowAsTowerWhenInvockedIsReachableByEnemyTowerThenReturnsTrue()
    {
        int row = 5;
        int column = 5;
        tablero = new TableroExtended();
        tablero.addWhiteTower(row, column);

        checkIfHorizontalTilesAreReachableByEnemyTower(false, row, column);
        row = 0;
        column = 0;
        tablero = new TableroExtended();
        tablero.addWhiteTower(row, column);

        Assert.True(tablero.isReachableByEnemyTower(false, row, column + 1));
        Assert.False(tablero.isReachableByEnemyTower(false, row, column - 1));
        Assert.True(tablero.isReachableByEnemyTower(false, row, column + 2));
        Assert.False(tablero.isReachableByEnemyTower(false, row, column - 2));
        row = 7;
        column = 7;
        tablero = new TableroExtended();
        tablero.addWhiteTower(row, column);
        Assert.False(tablero.isReachableByEnemyTower(false, row, column + 1));
        Assert.True(tablero.isReachableByEnemyTower(false, row, column - 1));
        Assert.False(tablero.isReachableByEnemyTower(false, row, column + 2));
        Assert.True(tablero.isReachableByEnemyTower(false, row, column - 2));

    }
    [Test]
    public void GivenAnyBlackTowerAndGivenAnyTileInSameRowAsTowerWhenInvockedIsReachableByEnemyTowerThenReturnsTrue()
    {
        int row = 5;
        int column = 5;
        tablero = new TableroExtended();
        tablero.addBlackTower(row, column);

        checkIfHorizontalTilesAreReachableByEnemyTower(true, row, column);
        row = 0;
        column = 0;
        tablero = new TableroExtended();
        tablero.addBlackTower(row, column);

        Assert.True(tablero.isReachableByEnemyTower(true, row, column + 1));
        Assert.False(tablero.isReachableByEnemyTower(true, row, column - 1));
        Assert.True(tablero.isReachableByEnemyTower(true, row, column + 2));
        Assert.False(tablero.isReachableByEnemyTower(true, row, column - 2));
        row = 7;
        column = 7;
        tablero = new TableroExtended();
        tablero.addBlackTower(row, column);
        Assert.False(tablero.isReachableByEnemyTower(true, row, column + 1));
        Assert.True(tablero.isReachableByEnemyTower(true, row, column - 1));
        Assert.False(tablero.isReachableByEnemyTower(true, row, column + 2));
        Assert.True(tablero.isReachableByEnemyTower(true, row, column - 2));

    }
    [Test]
    public void GivenAWhiteBishopOnWhiteTileAndGivenAnyTileAtSameDiagonalWhenInvockedIsReachableByEnemyBishopThenReturnsTrue()
    {
        int row = 3;
        int column = 3;
        TableroExtended tablero = new TableroExtended();
        tablero.addWhiteBishop(row, column);
        Assert.True(tablero.isReachableByEnemyBishop(false, row + 1, column + 1));
        Assert.True(tablero.isReachableByEnemyBishop(false, row + 2, column + 2));
        Assert.True(tablero.isReachableByEnemyBishop(false, row - 1, column - 1));
        Assert.True(tablero.isReachableByEnemyBishop(false, row -2, column -2));

        Assert.True(tablero.isReachableByEnemyBishop(false, row + 1, column - 1));
        Assert.True(tablero.isReachableByEnemyBishop(false, row + 2, column - 2));
        Assert.True(tablero.isReachableByEnemyBishop(false, row - 1, column + 1));
        Assert.True(tablero.isReachableByEnemyBishop(false, row - 2, column + 2));
    }
    [Test]
    public void GivenAWhiteBishopOnWhiteTileAndGivenAnyTileAtDifferentDiagonalWhenInvockedIsReachableByEnemyBishopThenReturnsFalse()
    {
        int row = 3;
        int column = 3;
        TableroExtended tablero = new TableroExtended();
        tablero.addWhiteBishop(row, column);
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 1, column + 2));
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 2, column + 3));
        Assert.False(tablero.isReachableByEnemyBishop(true, row - 1, column ));
        Assert.False(tablero.isReachableByEnemyBishop(true, row , column - 2));

        Assert.False(tablero.isReachableByEnemyBishop(true, row + 1, column ));
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 2, column - 3));
        Assert.False(tablero.isReachableByEnemyBishop(true, row - 1, column + 3));
        Assert.False(tablero.isReachableByEnemyBishop(true, row , column + 2));
    }
    [Test]
    public void GivenABlackBishopOnWhiteTileAndGivenAnyTileAtSameDiagonalWhenInvockedIsReachableByEnemyBishopThenReturnsTrue()
    {
        int row = 3;
        int column = 3;
        TableroExtended tablero = new TableroExtended();
        tablero.addBlackBishop(row, column);
        Assert.True(tablero.isReachableByEnemyBishop(true, row + 1, column + 1));
        Assert.True(tablero.isReachableByEnemyBishop(true, row + 2, column + 2));
        Assert.True(tablero.isReachableByEnemyBishop(true, row - 1, column - 1));
        Assert.True(tablero.isReachableByEnemyBishop(true, row - 2, column - 2));

        Assert.True(tablero.isReachableByEnemyBishop(true, row + 1, column - 1));
        Assert.True(tablero.isReachableByEnemyBishop(true, row + 2, column - 2));
        Assert.True(tablero.isReachableByEnemyBishop(true, row - 1, column + 1));
        Assert.True(tablero.isReachableByEnemyBishop(true, row - 2, column + 2));
    }

    [Test]
    public void GivenABlackBishopOnWhiteTileAndGivenAnyTileAtDifferentDiagonalWhenInvockedIsReachableByEnemyBishopThenReturnsFalse()
    {
        int row = 3;
        int column = 3;
        TableroExtended tablero = new TableroExtended();
        tablero.addWhiteBishop(row, column);
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 1, column + 2));
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 2, column + 3));
        Assert.False(tablero.isReachableByEnemyBishop(true, row - 1, column));
        Assert.False(tablero.isReachableByEnemyBishop(true, row, column - 2));
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 1, column));
        Assert.False(tablero.isReachableByEnemyBishop(true, row + 2, column - 3));
        Assert.False(tablero.isReachableByEnemyBishop(true, row - 1, column + 3));
        Assert.False(tablero.isReachableByEnemyBishop(true, row, column + 2));
    }
    [Test]
    public void GivenAnyWhiteKnightAtAnyTileAndGivenAnyTileReachableByThisKnightWhenInvockedIsReachableByEnemyKnightThenReturnsTrue()
    {
        int row = 5;
        int column = 5;
        TableroExtended tablero = new TableroExtended();
        tablero.addWhiteKnight(row, column);
        Assert.True(tablero.isReachableByEnemyKnight(false, row + 2, column + 1));
        Assert.True(tablero.isReachableByEnemyKnight(false, row + 2, column -1));
        Assert.True(tablero.isReachableByEnemyKnight(false, row - 2, column - 1));
        Assert.True(tablero.isReachableByEnemyKnight(false, row - 2, column +1));

        Assert.True(tablero.isReachableByEnemyKnight(false, row + 1, column - 2));
        Assert.True(tablero.isReachableByEnemyKnight(false, row  +1, column + 2));
        Assert.True(tablero.isReachableByEnemyKnight(false, row - 1, column -2));
        Assert.True(tablero.isReachableByEnemyKnight(false, row - 1, column + 2));
    }

    [Test]
    public void GivenAnyBlackKnightAtAnyTileAndGivenAnyTileReachableByThisKnightWhenInvockedIsReachableByEnemyKnightThenReturnsTrue()
    {
        int row = 5;
        int column = 5;
        TableroExtended tablero = new TableroExtended();
        tablero.addBlackKnight(row, column);
        Assert.True(tablero.isReachableByEnemyKnight(true, row + 2, column + 1));
        Assert.True(tablero.isReachableByEnemyKnight(true, row + 2, column - 1));
        Assert.True(tablero.isReachableByEnemyKnight(true, row - 2, column - 1));
        Assert.True(tablero.isReachableByEnemyKnight(true, row - 2, column + 1));

        Assert.True(tablero.isReachableByEnemyKnight(true, row + 1, column - 2));
        Assert.True(tablero.isReachableByEnemyKnight(true, row + 1, column + 2));
        Assert.True(tablero.isReachableByEnemyKnight(true, row - 1, column - 2));
        Assert.True(tablero.isReachableByEnemyKnight(true, row - 1, column + 2));
    }
    private void checkIfHorizontalTilesAreReachableByEnemyTower(bool turnoJugador, int towerRow, int towerColumn)
    {
        Assert.True(tablero.isReachableByEnemyTower(turnoJugador, towerRow, towerColumn + 1));
        Assert.True(tablero.isReachableByEnemyTower(turnoJugador, towerRow, towerColumn - 1));
        Assert.True(tablero.isReachableByEnemyTower(turnoJugador, towerRow, towerColumn + 2));
        Assert.True(tablero.isReachableByEnemyTower(turnoJugador, towerRow, towerColumn - 2));
    }

    [Test]
    public void GivenPlayer2TurnAndGivenBlackKingAt11WhiteQueenAt21AndWhiteKingAt31WhenInvokingHayJugadasThenReturnsFalse()
    {
        tablero = new TableroExtended();
        tablero.addBlackKing(0, 0);
        tablero.addWhiteQueen(1, 0);
        tablero.addWhiteKing(2, 0);
        bool hayJugadas = tablero.hayJugadas(false);
        Assert.False(hayJugadas);
    }

    [Test]
    public void GivenPlayer2TurnAndGivenBlackKingAt88WhiteQueenAt78AndWhiteKingAt68WhenInvokingHayJugadasThenReturnsFalse()
    {
        tablero = new TableroExtended();
        tablero.addBlackKing(7, 7);
        tablero.addWhiteQueen(6, 7);
        tablero.addWhiteKing(5, 7);
        bool hayJugadas = tablero.hayJugadas(false);
        Assert.False(hayJugadas);
    }
    [Test]
    public void GivenPlayer1TurnAndGivenWhiteKingAt88BlackQueenAt78AndBlackKingAt68WhenInvokingHayJugadasThenReturnsFalse()
    {
        tablero = new TableroExtended();
        tablero.addWhiteKing(7, 7);
        tablero.addBlackQueen(6, 7);
        tablero.addBlackKing(5, 7);
        bool hayJugadas = tablero.hayJugadas(true);
        Assert.False(hayJugadas);
    }

    [Test]
    public void GivenPlayer2TurnAndGivenBlackKingAt87WhiteQueenAt85AndBlackPawnsAt78At77AndAt76WhenInvokingHayJugadasThenReturnsFalse()
    {//no funcionará hasta que no se obligue a todas las piezas a proteger al rey, cuando hay jaque
        tablero = new TableroExtended();
        tablero.addBlackKing(0, 6);
        tablero.addWhiteQueen(0, 4);
        tablero.addBlackPawn(1, 7);
        tablero.addBlackPawn(1, 6);
        tablero.addBlackPawn(1, 5);
        bool hayJugadas = tablero.hayJugadas(false);
        Assert.False(hayJugadas);
    }

    [Test]
    public void GivenPlayer2TurnAndGivenBlackKingAt87WhiteQueenAt85AndWhiteTowerAt71WhenInvokingHayJugadasThenReturnsFalse()
    {
        tablero = new TableroExtended();
        tablero.addBlackKing(7, 6);
        tablero.addWhiteQueen(7, 4);
        tablero.addWhiteTower(6, 0);
        bool hayJugadas = tablero.hayJugadas(false);
        Assert.False(hayJugadas);
    }
    [Test]
    public void GivenPlayer1TurnAndGivenTheStartingPositionForAllPiecesWhenInvokingHayJugadasThenReturnsTrue()
    {
        tablero = new TableroExtended();
        tablero.addBlackTower(0, 0);
        tablero.addBlackKnight(0, 1);
        tablero.addBlackBishop(0, 2);
        tablero.addBlackKing(0, 3);
        tablero.addBlackQueen(0, 4);
        tablero.addBlackBishop(0, 5);
        tablero.addBlackKnight(0, 6);
        tablero.addBlackTower(0, 7);
        tablero.addBlackPawn(1, 0);
        tablero.addBlackPawn(1, 1);
        tablero.addBlackPawn(1, 2);
        tablero.addBlackPawn(1, 3);
        tablero.addBlackPawn(1, 4);
        tablero.addBlackPawn(1, 5);
        tablero.addBlackPawn(1, 6);
        tablero.addBlackPawn(1, 7);

        tablero.addWhiteTower(7, 0);
        tablero.addWhiteKnight(7, 1);
        tablero.addWhiteBishop(7, 2);
        tablero.addWhiteKing(7, 3);
        tablero.addWhiteQueen(7, 4);
        tablero.addWhiteBishop(7, 5);
        tablero.addWhiteKnight(7, 6);
        tablero.addWhiteTower(7, 7);
        tablero.addWhitePawn(6, 0);
        tablero.addWhitePawn(6, 1);
        tablero.addWhitePawn(6, 2);
        tablero.addWhitePawn(6, 3);
        tablero.addWhitePawn(6, 4);
        tablero.addWhitePawn(6, 5);
        tablero.addWhitePawn(6, 6);
        tablero.addWhitePawn(6, 7);
        bool hayJugadas = tablero.hayJugadas(true);
        Assert.True(hayJugadas);
    }

    [Test]
    public void GivenKingWhoHasntMovedAndIsNotCheckedAndLeftRookWhoHasntMovedAndGivenNoThreatenedTilesBetweenThemAndGivenNoThreatenedTilesBetweenThemAndGivenCastlingMoveWhenInvokingComprobarValidezJugadaThenReturnsTrue()
    {
        tablero = new TableroExtended();
        tablero.addBlackTower(0, 0);
       PiezaVirtual rey = tablero.addBlackKing(0, 4);
        bool jugadaValida = rey.comprobarValidezJugada(new int[] { rey.getPosX(), rey.getPosY() }, new int[] { rey.getPosX() , rey.getPosY() -2 }, tablero);
        Assert.True(jugadaValida);

    }
    [Test]
    public void GivenKingWhoHasntMovedAndIsNotCheckedAndLeftRookWhoHasntMovedAndGivenOneThreatenedTileBetweenThemAndGivenNoThreatenedTilesBetweenThemAndGivenCastlingMoveWhenInvokingComprobarValidezJugadaThenReturnsFalse()
    {
        tablero = new TableroExtended();
        tablero.addBlackTower(0, 0);
        PiezaVirtual rey =tablero.addBlackKing(0, 4);
        tablero.addWhiteTower(5, 1);
        bool jugadaValida = rey.comprobarValidezJugada(new int[] { rey.getPosX(), rey.getPosY() }, new int[] { rey.getPosX() , rey.getPosY() -2 }, tablero);
        Assert.False(jugadaValida);

    }
    [Test]
    public void GivenKingWhoHasMovedAndIsNotCheckedAndLeftRookWhoHasntMovedAndGivenNoThreatenedTilesBetweenThemAndGivenCastlingMoveWhenInvokingComprobarValidezJugadaThenReturnsFalse()
    {
        tablero = new TableroExtended();
        tablero.addBlackTower(0, 0);
        PiezaVirtual rey = tablero.addBlackKing(0, 4);
        rey.setPiezaMovida(true);
        
        bool jugadaValida = rey.comprobarValidezJugada(new int[] { rey.getPosX(), rey.getPosY() }, new int[] { rey.getPosX(), rey.getPosY() -2 }, tablero);
        Assert.False(jugadaValida);

    }

    [Test]
    public void GivenKingWhoHasntMovedAndIsNotCheckedAndLeftRookWhoHasMovedAndGivenNoThreatenedTilesBetweenThemAndGivenNoThreatenedTilesBetweenThemAndGivenCastlingMoveWhenInvokingComprobarValidezJugadaThenReturnsFalse()
    {
        tablero = new TableroExtended();
        PiezaVirtual torre = tablero.addBlackTower(0, 0);
        PiezaVirtual rey = tablero.addBlackKing(0, 4);
        torre.setPiezaMovida(true);
        bool jugadaValida = rey.comprobarValidezJugada(new int[] { rey.getPosX(), rey.getPosY() }, new int[] { rey.getPosX(), rey.getPosY() -2 }, tablero);
        Assert.False(jugadaValida);

    }
    [Test]
    public void GivenCastlingMoveWithKingWhenInvockedEsEnroqueThenReturnsTrue()
    {

    }

    private class TableroExtended : TableroV
    {

        public TableroExtended()
        {
            matrizPiezasV = new PiezaVirtual[8, 8];
        }
        private void configureVirtualPiece(PiezaVirtual piece, int row, int column, bool owner)
        {
            piece.setOwner(owner);
            piece.setMatrixIndexes(row, column);
            matrizPiezasV[row, column] = piece;
        }
        private void configureVirtualWhitePiece(PiezaVirtual piece, int row, int column)
        {
            configureVirtualPiece(piece, row, column, true);
            getListaPiezasJBlanco().Add(piece);
        }
        private void configureVirtualBlackPiece(PiezaVirtual piece, int row, int column)
        {
            configureVirtualPiece(piece, row, column, false);
            getListaPiezasJNegro().Add(piece);
        }
        public void addWhitePawn(int row, int column)
        {
            PeonV pawn = new PeonV();
            configureVirtualWhitePiece(pawn, row, column);
        }
        public void addBlackPawn(int row, int column)
        {
            PeonV pawn = new PeonV();
            configureVirtualBlackPiece(pawn, row, column);
        }
        public void addWhiteTower(int row, int column)
        {
            TorreV tower = new TorreV();
            configureVirtualWhitePiece(tower, row, column);
        }
        public PiezaVirtual addBlackTower(int row, int column)
        {
            TorreV tower = new TorreV();
            configureVirtualBlackPiece(tower, row, column);
            return tower;
        }
        public void addWhiteBishop(int row, int column)
        {
            AlfilV bishop = new AlfilV();
            configureVirtualWhitePiece(bishop, row, column);
        }
        public void addBlackBishop(int row, int column)
        {
            AlfilV bishop = new AlfilV();
            configureVirtualBlackPiece(bishop, row, column);
        }
        public void addWhiteKnight(int row, int column)
        {
            CaballoV knight = new CaballoV();
            configureVirtualWhitePiece(knight, row, column);
        }
        public void addBlackKnight(int row, int column)
        {
            CaballoV knight = new CaballoV();
            configureVirtualBlackPiece(knight, row, column);
        }
        public void addWhiteQueen(int row, int column)
        {
            ReinaV queen = new ReinaV();
            configureVirtualWhitePiece(queen, row, column);
        }
        public void addBlackQueen(int row, int column)
        {
            ReinaV queen = new ReinaV();
            configureVirtualBlackPiece(queen, row, column);
        }
        public void addWhiteKing(int row, int column)
        {
            ReyV king = new ReyV();
            configureVirtualWhitePiece(king, row, column);
            this.setReyBlanco(king);
        }
        public PiezaVirtual addBlackKing(int row, int column)
        {
            ReyV king = new ReyV();
            configureVirtualBlackPiece(king, row, column);
            this.setReyNegro(king);
            return king;
        }
    }
}
